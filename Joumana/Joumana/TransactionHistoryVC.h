//
//  TransactionHistoryVC.h
//  Joumana
//
//  Created by Ahmed Sadiq on 25/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface TransactionHistoryVC : UIViewController <UITableViewDelegate,UITableViewDataSource>
@property (strong, nonatomic) IBOutlet UITableView *transTbl;
@property (strong, nonatomic) AppDelegate *delegate;

- (IBAction)backPressed:(id)sender;


@end

//
//  HomeVC.m
//  Joumana
//
//  Created by Ahmed Sadiq on 20/09/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "HomeVC.h"
#import "NewsVC.h"
#import "Utils.h"
#import "GalleryVC.h"
#import "BookCallVC.h"
#import "SettingsVC.h"
#import "AppDelegate.h"
#import "DailyReadingVC.h"
#import "CustomLoading.h"
#import "ViewController.h"
#import "StarsCollectionView.h"
#import "InternalTableView.h"
#import "PostModel.h"



#import <QuartzCore/QuartzCore.h>

@interface HomeVC ()<UIWebViewDelegate>
{
   BOOL isWebViewLoaded;
   
}
@end

//#define kPurchaseContentProductIdentifier @"com.joumana.monthlycontent"
#define kPurchaseContentProductIdentifier @"com.joumana.yearlycontent"
#define sandBoxUrl             @"https://sandbox.itunes.apple.com/verifyReceipt"
#define BuyItuneUrl              @"https://buy.itunes.apple.com/verifyReceipt"

@implementation HomeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    if(!appDel.sharedUserModel) {
       // _btngallery.hidden = YES;
        [self getHomeContent];
    }
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)webViewDidFinishLoad:(UIWebView *)webView{
    isWebViewLoaded =YES;
    [CustomLoading DismissAlertMessage];

}
-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    [CustomLoading DismissAlertMessage];
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
- (IBAction)bnBackFromPrivacy:(id)sender {
    _privacyView.hidden = YES;

}
- (IBAction)btnHideSubscribeOptionView:(id)sender {
    _subscripeOptionView.hidden = YES;
}
- (IBAction)callBtnPressed:(id)sender {
    BOOL isLoggedIn = [[NSUserDefaults standardUserDefaults] boolForKey:@"isLoggedIn"];
    if(isLoggedIn) {
        BookCallVC *bookCall = [[BookCallVC alloc] initWithNibName:@"BookCallVC" bundle:nil];
        [self.navigationController pushViewController:bookCall animated:YES];
        [self.navigationController setNavigationBarHidden:YES];
    }
    else {
        ViewController *bookCall = [[ViewController alloc] initWithNibName:@"ViewController" bundle:nil];
        [self.navigationController pushViewController:bookCall animated:YES];
        [self.navigationController setNavigationBarHidden:YES];
    }
}



- (IBAction)newspressed:(id)sender {
    NewsVC *newsVC = [[NewsVC alloc] initWithNibName:@"NewsVC" bundle:nil];
    [self.navigationController pushViewController:newsVC animated:YES];
    [self.navigationController setNavigationBarHidden:YES];
}
- (IBAction)galleryBtnPressed:(id)sender {
    
    //[_subWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://joumana-koubaissi.com/"]]];

    //////////////////////////////////////////////////// new code
    [self checkInAppPurchaseStatus];
    
    
}
- (IBAction)btnYearlyExpections:(id)sender {
    [self getYearlyExpectations];
}

- (IBAction)btnDailyHoroscope:(id)sender {
    StarsCollectionView *star = [[StarsCollectionView alloc] initWithNibName:@"StarsCollectionView" bundle:nil];
    star.option = 10;
    [self.navigationController pushViewController:star animated:YES];
    [self.navigationController setNavigationBarHidden:YES];
}


- (IBAction)dailyReadingPressed:(id)sender {
    /*DailyReadingVC *dailyReading = [[DailyReadingVC alloc] initWithNibName:@"DailyReadingVC" bundle:nil];
    [self.navigationController pushViewController:dailyReading animated:YES];
    [self.navigationController setNavigationBarHidden:YES];*/
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"https://www.facebook.com/joumanakoubaissi/"]];
    
}
- (IBAction)settingsPressed:(id)sender {
    BOOL isLoggedIn = [[NSUserDefaults standardUserDefaults] boolForKey:@"isLoggedIn"];
    if(isLoggedIn) {
        SettingsVC *settingsVC = [[SettingsVC alloc] initWithNibName:@"SettingsVC" bundle:nil];
        [self.navigationController pushViewController:settingsVC animated:YES];
        [self.navigationController setNavigationBarHidden:YES];
    }
    else {
        ViewController *bookCall = [[ViewController alloc] initWithNibName:@"ViewController" bundle:nil];
        [self.navigationController pushViewController:bookCall animated:YES];
        [self.navigationController setNavigationBarHidden:YES];
    }
    
    
}

- (void)getHomeContent {
    
    AppDelegate *del = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [CustomLoading showAlertMessage];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];

    NSString *date_time = [NSString stringWithFormat:@"%@", [dateFormatter stringFromDate: [NSDate date]]];
    
    NSString *userID = (NSString*)[[NSUserDefaults standardUserDefaults] objectForKey:@"userID"];
    
    if(!userID) {
        userID = @"1446";
    }
    NSString *secondPartOfUrl = [NSString stringWithFormat:@"%@%@",@"User/get_home_content2?user_id=",userID];
    
    
    
    NSString *urlStr = [NSString stringWithFormat:@"%@%@",BASE_URL,secondPartOfUrl];
    
    //urlStr = [NSString stringWithFormat:@"%@&date_time=%@",urlStr,date_time];
    if(del.deviceToken) {
        urlStr = [NSString stringWithFormat:@"%@&device_id=%@",urlStr,del.deviceToken];
        urlStr = [NSString stringWithFormat:@"%@&type=%@",urlStr,@"2"];
    }
    
    urlStr = [urlStr stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    NSURL *url = [NSURL URLWithString:urlStr];
    
    // Create the request.
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    // Create url connection and fire request
    NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    //[CustomLoading showAlertMessage];
}

#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    // A response has been received, this is where we initialize the instance var you created
    // so that we can append data to it in the didReceiveData method
    // Furthermore, this method is called each time there is a redirect so reinitializing it
    // also serves to clear it
    _responseData = [[NSMutableData alloc] init];
    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    // Append the new data to the instance variable you declared
    [_responseData appendData:data];
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    // Return nil to indicate not necessary to store a cached response for this connection
    return nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    // The request is complete and data has been received
    // You can parse the stuff in your instance variable now
    
    [CustomLoading DismissAlertMessage];
    
    NSDictionary *result = [NSJSONSerialization JSONObjectWithData:_responseData options:0 error:nil];
    int flag = [[result objectForKey:@"status"] intValue];
    
    if(flag == 1) {
        
        User *user = [[User alloc] initWithDictionary:[result objectForKey:@"user_data"]];
        [user populateEvents:(NSArray*)[result objectForKey:@"events"]];
        [user populateBookedCallsTime:(NSArray*)[result objectForKey:@"user_specific_booked_calls"]];
        [user populateZodiacStarsArray:(NSArray*)[result objectForKey:@"hoscopesro"]];
        [user populateNewsArray:(NSArray*)[result objectForKey:@"news"]];
        [user populatePicturesArray:(NSArray*)[result objectForKey:@"pictures"]];
        [user populateWorkingHoursArray:(NSArray*)[result objectForKey:@"working_hours"]];
        [user populateVideoArray:(NSArray*)[result objectForKey:@"videos"]];
        [user populateTransactionArray:(NSArray*)[result objectForKey:@"transactions_history"]];
        CreditCard *ccObj = [[CreditCard alloc] initWithDictionary:[result objectForKey:@"credit_card_info"]];
        user.creditCardObj = ccObj;
        
        AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
        appDel.sharedUserModel = user;
        appDel.subscriptionUrl = @"";
        
    }
    else {
        
        [self presentViewController:[Utils showCustomAlert:@"حدث خطأ ما" andMessage:[result objectForKey:@"message"]] animated:YES completion:nil];
        
    }
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // The request has failed for some reason!
    // Check the error var
    [CustomLoading DismissAlertMessage];
    
    [self presentViewController:[Utils showCustomAlert:@"حدث خطأ ما" andMessage:@"There seems to be some problem with the connection"] animated:YES completion:nil];
}

#pragma mark - Subsription Methods


- (IBAction)subscripeBtnPressed:(id)sender {
    [CustomLoading showAlertMessage];
    [self makePurchase];
    
}

-(void) goToSubscripedViews {
//    GalleryVC *galleryVC = [[GalleryVC alloc] initWithNibName:@"GalleryVC" bundle:nil];
//    [self.navigationController pushViewController:galleryVC animated:YES];
//    [self.navigationController setNavigationBarHidden:YES];
    dispatch_async(dispatch_get_main_queue(), ^{
        
        // do work here

        _subscripeOptionView.hidden = NO;
        _subscriptionView.hidden = YES;
    });
    
}

- (IBAction)cancelBtnPressed:(id)sender {
    _subscriptionView.hidden = true;
}

- (IBAction)subscriptionDetails:(id)sender {
   // _subscriptionHelpView.hidden = false;
    if (!isWebViewLoaded) {
        [CustomLoading showAlertMessage];
    }

       [_subWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"https://joumana.witsapplication.com/PrivacyPolicy"]]];
    _privacyView.hidden = NO;

}

- (void) makePurchase {
    if([SKPaymentQueue canMakePayments]){
        
        SKProductsRequest *productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:[NSSet setWithObject:kPurchaseContentProductIdentifier]];
        productsRequest.delegate = self;
        [productsRequest start];
        
    }
    else{
        NSLog(@"User cannot make payments due to parental controls");
        //this is called the user cannot make payments, most likely due to parental controls
    }
}


- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response{
    SKProduct *validProduct = nil;
    int count = [response.products count];
    if(count > 0){
        validProduct = [response.products objectAtIndex:0];
        NSLog(@"Products Available!");
        [self purchase:validProduct];
    }
    else if(!validProduct){
        NSLog(@"No products available");
        //this is called if your product id is not valid, this shouldn't be called unless that happens.
    }
}

- (void)purchase:(SKProduct *)product{
    SKPayment *payment = [SKPayment paymentWithProduct:product];
    
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    [[SKPaymentQueue defaultQueue] addPayment:payment];
}

- (IBAction) restore{
    //this is called when the user restores purchases, you should hook this up to a button
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
}

- (void) paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue
{
    NSLog(@"received restored transactions: %i", queue.transactions.count);
    for(SKPaymentTransaction *transaction in queue.transactions){
        if(transaction.transactionState == SKPaymentTransactionStateRestored){
            //called when the user successfully restores a purchase
            NSLog(@"Transaction state -> Restored");
            
            //give access to content of appliation
            [CustomLoading DismissAlertMessage];
            
            
            [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
            
            [self goToSubscripedViews];
            break;
        }
    }
}
- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions{
    for(SKPaymentTransaction *transaction in transactions){
        switch(transaction.transactionState){
            case SKPaymentTransactionStatePurchasing: NSLog(@"Transaction state -> Purchasing");
                //called when the user is in the process of purchasing, do not add any of your own code here.
                break;
            case SKPaymentTransactionStatePurchased:
                //this is called when the user has successfully purchased the package (Cha-Ching!)
                //[self doRemoveAds]; //you can add your code for what you want to happen when the user buys the purchase here, for this tutorial we use removing ads
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                NSLog(@"Transaction state -> Purchased");
                
                [CustomLoading DismissAlertMessage];
                //give access to content of appliation
                [self goToSubscripedViews];
                
                break;
            case SKPaymentTransactionStateRestored:
                NSLog(@"Transaction state -> Restored");
                //add the same code as you did from SKPaymentTransactionStatePurchased here
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                
                //give access to content of appliation
                [CustomLoading DismissAlertMessage];
                [self goToSubscripedViews];
                
                break;
            case SKPaymentTransactionStateFailed:
                
                [CustomLoading DismissAlertMessage];
                
                //called when the transaction does not finish
                if(transaction.error.code != SKErrorPaymentCancelled){
                    NSLog(@"Transaction error: %@", transaction.error.localizedDescription);
                    //the user cancelled the payment ;(
                }
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                break;
        }
    }
}

- (void)checkInAppPurchaseStatus
{
    
    // Load the receipt from the app bundle.
    NSURL *receiptURL = [[NSBundle mainBundle] appStoreReceiptURL];
    NSData *receipt = [NSData dataWithContentsOfURL:receiptURL];
    if (!receipt) { /* No local receipt -- handle the error. */
        [self showSubscripedView];
        
        return;
    }
    
    /* ... Send the receipt data to your server ... */

    
    // Create the JSON object that describes the request
    NSError *error;
    NSDictionary *requestContents = @{
                                      @"receipt-data": [receipt base64EncodedStringWithOptions:0],
                                      @"password":@"cde328f413b84befaef21dcb8c1fa7ca"
                                      };
    NSData *requestData = [NSJSONSerialization dataWithJSONObject:requestContents
                                                          options:0
                                                            error:&error];
    [CustomLoading showAlertMessage];
    if (!requestData) { /* ... Handle error ... */ }
    
    // Create a POST request with the receipt data.
    NSURL *storeURL = [NSURL URLWithString:BuyItuneUrl];
    NSMutableURLRequest *storeRequest = [NSMutableURLRequest requestWithURL:storeURL];
    [storeRequest setHTTPMethod:@"POST"];
    [storeRequest setHTTPBody:requestData];
    
    // Make a connection to the iTunes Store on a background queue.
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:storeRequest queue:queue
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                               if (connectionError) {
                                   [CustomLoading DismissAlertMessage];
                                   /* ... Handle error ... */
                                   
                               } else {
                                   NSError *error;
                                   NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                                   if (!jsonResponse) { /* ... Handle error ...*/
                                   
                                   }
                                   else{
                                       NSString *str = [jsonResponse objectForKey:@"status"];
                                       if ([str intValue] == 21007) {
                                           [self sandBoxEnvironmentCall];
                                       }
                                       else if([str intValue] == 0) {
                                           dispatch_async(dispatch_get_main_queue(), ^{
                                               [CustomLoading DismissAlertMessage];

                                           });
                                                               [self goToSubscripedViews];
                                                    }
                                       
                                       
                                       else {
                                           dispatch_async(dispatch_get_main_queue(), ^{
                                               [CustomLoading DismissAlertMessage];
                                               
                                           });                                                               [self showSubscripedView];
                                           
                                       }
                                       
                                   }
                                   /* ... Send a response back to the device ... */
                               }
                           }];

    
    
}


-(void) sandBoxEnvironmentCall{
    NSURL *receiptURL = [[NSBundle mainBundle] appStoreReceiptURL];
    NSData *receipt = [NSData dataWithContentsOfURL:receiptURL];
    if (!receipt) { /* No local receipt -- handle the error. */
        [self showSubscripedView];
        [CustomLoading DismissAlertMessage];
        return;
    }
    
    /* ... Send the receipt data to your server ... */
    
    
    // Create the JSON object that describes the request
    NSError *error;
    NSDictionary *requestContents = @{
                                      @"receipt-data": [receipt base64EncodedStringWithOptions:0],
                                      @"password":@"cde328f413b84befaef21dcb8c1fa7ca"
                                      };
    NSData *requestData = [NSJSONSerialization dataWithJSONObject:requestContents
                                                          options:0
                                                            error:&error];
    
    if (!requestData) { /* ... Handle error ... */ }
    
    // Create a POST request with the receipt data.
    NSURL *storeURL = [NSURL URLWithString:sandBoxUrl];
    NSMutableURLRequest *storeRequest = [NSMutableURLRequest requestWithURL:storeURL];
    [storeRequest setHTTPMethod:@"POST"];
    [storeRequest setHTTPBody:requestData];
    
    // Make a connection to the iTunes Store on a background queue.
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:storeRequest queue:queue
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                               dispatch_async(dispatch_get_main_queue(), ^{
                                   [CustomLoading DismissAlertMessage];
                                   
                               });
                               if (connectionError) {
                                   /* ... Handle error ... */
                                   
                               } else {
                                   NSError *error;
                                   NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                                   if (!jsonResponse) { /* ... Handle error ...*/
                                       
                                   }
                                   else{
                                       NSString *str = [jsonResponse objectForKey:@"status"];
                                     
                                    if([str intValue] == 0) {
                                           [self goToSubscripedViews];
                                       }
                                       
                                       
                                       else {
                                           [self showSubscripedView];
                                           
                                       }
                                       
                                   }
                                   /* ... Send a response back to the device ... */
                               }
                           }];
    


}

- (void) showSubscripedView {
    [CustomLoading DismissAlertMessage];
    _subscriptionView.hidden = false;
    
    [_subscriptionView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [_subscriptionView.layer setBorderWidth:1.0f];
}

- (IBAction)subscriptionCancelPressed:(id)sender {
    _subscriptionHelpView.hidden = true;
}

- (IBAction)visitPrivacyPolicy:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://privacypolicy.witsapplication.com/privacyPolicy.html"]];

}

- (IBAction)aboutSubscriptionPressed:(id)sender {
    _subscriptionHelpView.hidden = false;
}
- (IBAction)subBackPressed:(id)sender {
    
    _subscriptionView.hidden = true;
}


-(void) getYearlyExpectations{
    _btnYearly.enabled= NO;
    NSString *strSecondPart = [NSString stringWithFormat:@"/Post/get_yearly_posts?"];
    NSString *urlStr = [NSString stringWithFormat:@"%@%@",BASE_URL,strSecondPart];
    NSURL *url = [NSURL URLWithString:urlStr];
    [CustomLoading showAlertMessage];

    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"GET"];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        _btnYearly.enabled= YES;

        [CustomLoading DismissAlertMessage];
        
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int flag = [[result objectForKey:@"status"] intValue];
            if(flag == 1) {
                self.postArray = [[NSMutableArray alloc] init];
                NSArray *postArray = [result objectForKey:@"posts"];
                for(int i=0; i<postArray.count; i++) {
                    NSDictionary *postDict = [postArray objectAtIndex:i];
                    [self.postArray addObject:[[PostModel alloc] initWithDictionary:postDict]];
                }
                
                
                InternalTableView *starViewCont = [[InternalTableView alloc] initWithNibName:@"InternalTableView" bundle:nil];
                starViewCont.postArray = self.postArray;
                starViewCont.option = 20;

                [self.navigationController pushViewController:starViewCont animated:YES];
                [self.navigationController setNavigationBarHidden:YES];
                
            }
        
        
            else {
                
                [self presentViewController:[Utils showCustomAlert:@"حدث خطأ ما" andMessage:[result objectForKey:@"message"]] animated:YES completion:nil];
            }
        }
        else{
            
            [self presentViewController:[Utils showCustomAlert:@"Conection Error" andMessage:@"Please retry after some time"] animated:YES completion:nil];
        }
    }];
    
    
}




@end

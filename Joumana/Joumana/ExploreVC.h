//
//  ExploreVC.h
//  Joumana
//
//  Created by Ahmed Sadiq on 20/09/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExploreVC : UIViewController<UIGestureRecognizerDelegate>
@property (strong, nonatomic) IBOutlet UIImageView *thumbnail;
@property (strong, nonatomic) IBOutlet UIImageView *thumbnail2;
@property (strong, nonatomic) IBOutlet UIScrollView *mainScroller;

@property (strong, nonatomic) IBOutlet UIPageControl *pager;
@end

//
//  SignInVC.h
//  Joumana
//
//  Created by Ahmed Sadiq on 20/09/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "NIDropDown.h"

@interface SignInVC : UIViewController <UITextFieldDelegate,UIGestureRecognizerDelegate,NIDropDownDelegate> {
    NSString *emailStr;
    NSString *name;
    NSString *profilePicUrl;
    NSString *gender;
    
    BOOL isLoginFromNumber;
    UITapGestureRecognizer *tap ;
    
    NIDropDown *dropDown;
    IBOutlet UIButton *optionBtn;
    
}
- (IBAction)logInOptionPressed:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *email;
@property (strong, nonatomic) IBOutlet UITextField *pswd;

- (IBAction)fbPressed:(id)sender;

@end

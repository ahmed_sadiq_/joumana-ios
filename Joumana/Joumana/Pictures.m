//
//  Pictures.m
//  Joumana
//
//  Created by Ahmed Sadiq on 18/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "Pictures.h"

@implementation Pictures

- (id)initWithDictionary:(NSDictionary *) responseData
{
    
    self = [super init];
    
    if (self) {
        
        self.date_time = [self validStringForObject:responseData [@"date_time"]];
        self.picture  = [self completeImageURL:responseData [@"picture"]];
        self.picture_id  = [self validStringForObject:responseData [@"picture_id"]];
        
    }
    
    return self;
}
@end

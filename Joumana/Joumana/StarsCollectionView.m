//
//  StarsCollectionView.m
//  Joumana
//
//  Created by Ahmed Sadiq on 22/12/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "StarsCollectionView.h"
#import "StarViewController.h"
#import "CustomLoading.h"
#import "AppDelegate.h"
#import "ZodiacStar.h"
#import "Utils.h"
#import "PostModel.h"
#import "SubInternalVC.h"

@interface StarsCollectionView ()

@end

@implementation StarsCollectionView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    if(_option == 1) {
        _titleLbl.text = @"القراءات اليومية";
    }
    else if(_option == 2) {
        _titleLbl.text = @"القراءات الأسبوعية";
    }
    else if(_option == 3) {
        _titleLbl.text = @"أرقام الحظ";
    }
    else if(_option == 10){
        _titleLbl.text = @"الأبراج اليومية";

    }
    else {
        _titleLbl.text = @"نصيحة";
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)starPressed:(id)sender {
    
    AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    
    UIButton *btnSender = (UIButton*)sender;
    
    ZodiacStar *zStar = [delegate.sharedUserModel.zodiacStarsArray objectAtIndex:(btnSender.tag)-1];
    
    
    StarViewController *starViewCont = [[StarViewController alloc] initWithNibName:@"StarViewController" bundle:nil];
    starViewCont.currentStar = zStar;
    starViewCont.option = _option;
    starViewCont.datesArray = self.datesCollection;
    [self.navigationController pushViewController:starViewCont animated:YES];
    [self.navigationController setNavigationBarHidden:YES];

    
    ////////////////////////////////////////
//    [CustomLoading showAlertMessage];
//    
//    NSString *strSecondPart = [NSString stringWithFormat:@"Post/get_dates?type=1&star_id=%@",zStar.starId];
//    NSString *urlStr = [NSString stringWithFormat:@"%@%@",BASE_URL,strSecondPart];
//    NSURL *url = [NSURL URLWithString:urlStr];
//    
//    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
//    [request setURL:url];
//    [request setHTTPMethod:@"GET"];
//    
//    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
//        
//        [CustomLoading DismissAlertMessage];
//        
//        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
//        {
//            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
//            int flag = [[result objectForKey:@"status"] intValue];
//            
//            if(flag == 1) {
//                
//                NSArray *datesArray = [result objectForKey:@"dates"];
//                self.datesCollection = [[NSMutableArray alloc] init];
//                for(int i=0; i<datesArray.count; i++) {
//                    NSMutableDictionary *innerDict = [datesArray objectAtIndex:i];
//                    NSString *dateStr = [innerDict objectForKey:@"date"];
//                    
//                    [self.datesCollection addObject:dateStr];
//                    
//                }
//                
//                StarViewController *starViewCont = [[StarViewController alloc] initWithNibName:@"StarViewController" bundle:nil];
//                starViewCont.currentStar = zStar;
//                starViewCont.option = _option;
//                starViewCont.datesArray = self.datesCollection;
//                [self.navigationController pushViewController:starViewCont animated:YES];
//                [self.navigationController setNavigationBarHidden:YES];
//                
//                
//            }
//            else {
//                
//                [self presentViewController:[Utils showCustomAlert:@"حدث خطأ ما" andMessage:[result objectForKey:@"message"]] animated:YES completion:nil];
//            }
//        }
//        else{
//            
//            [self presentViewController:[Utils showCustomAlert:@"Conection Error" andMessage:@"Please retry after some time"] animated:YES completion:nil];
//        }
//    }];
//        

        
}

- (IBAction)backPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

@end

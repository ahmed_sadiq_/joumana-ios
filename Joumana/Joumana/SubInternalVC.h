//
//  SubInternalVC.h
//  Joumana
//
//  Created by Ahmed Sadiq on 22/12/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PostModel.h"
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import <QuartzCore/QuartzCore.h>
#import <MediaPlayer/MediaPlayer.h>
#import "GUIPlayerView.h"

@interface SubInternalVC : UIViewController<GUIPlayerViewDelegate> {
    
}
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollViewTopConstrain;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollViewHeightConstrain;
@property (strong, nonatomic) IBOutlet UILabel *tLbl;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titLeTopConstrain;
@property (weak, nonatomic) IBOutlet UITextView *tfDesc;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleHeightConstrain;
@property int option;
@property (strong, nonatomic) IBOutlet UIScrollView *mainScrollView;
@property (strong, nonatomic) IBOutlet UILabel *titleLbl;
@property (strong, nonatomic) IBOutlet UILabel *descLbl;
@property (strong, nonatomic) PostModel *pModel;
@property (strong, nonatomic) NSMutableArray *players;
@property (nonatomic, strong) UIView *topView;
- (IBAction)backPressed:(id)sender;
@end

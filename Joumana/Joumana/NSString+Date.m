//
//  NSString+Date.m
//  Maggie
//
//  Created by Samreen on 01/05/2017.
//  Copyright © 2017 Hannan Khan. All rights reserved.
//

#import "NSString+Date.h"

@implementation NSString (Date)
-(NSString *) convertDateINtoLocalTimezone:(NSString *)dateStr{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    
    NSDate *date = [dateFormatter dateFromString:dateStr]; // create date from string
    
    // change to a readable time format and change to local time zone
    [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    NSString *timestamp = [dateFormatter stringFromDate:date];
    return timestamp;
}

-(NSString *) convertDateINtoLocalTimezoneWithFormate:(NSString *)formate andDate:(NSString *)dateStr{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    
    NSDate *date = [dateFormatter dateFromString:dateStr]; // create date from string
    
    // change to a readable time format and change to local time zone
    [dateFormatter setDateFormat:formate];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    NSString *timestamp = [dateFormatter stringFromDate:date];
    return timestamp;
}

-(NSString *) getCallTimeWithStartTime:(NSString *)start andEndTime:(NSString *)end{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    NSDate *sDate = [dateFormatter dateFromString:start]; // create date from string
    NSDate *eDate = [dateFormatter dateFromString:end]; // create date from string

    // change to a readable time format and change to local time zone
    [dateFormatter setDateFormat:@"h:mm a"];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    NSString *timestampStart = [dateFormatter stringFromDate:sDate];
    NSString *timestampEnd = [dateFormatter stringFromDate:eDate];
    NSString *calTime = [NSString stringWithFormat:@"%@ to %@",timestampStart,timestampEnd];
    return calTime;
}
-(NSString *) getRemaningTimeWithCallStartTime:(NSString *)start{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    NSDate *sDate = [dateFormatter dateFromString:start]; // create date from string
    
    NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
    [dateFormatter2 setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    [dateFormatter2 setTimeZone:[NSTimeZone localTimeZone]];

    dateFormatter2.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];

    NSString *currentTime = [dateFormatter2 stringFromDate:[NSDate date]];

    NSDate *now = [dateFormatter dateFromString:currentTime];
  
    NSTimeInterval distanceBetweenDates = [sDate timeIntervalSinceDate:now];
    double secondsInAnHour = 3600;
    //NSInteger hoursBetweenDates = distanceBetweenDates / secondsInAnHour;
    NSInteger min = distanceBetweenDates/60;

    return [NSString stringWithFormat:@"%ld",(long)min];
}

-(NSString *) getCalldurationWithEndTime:(NSString *)end{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    NSDate *sDate = [dateFormatter dateFromString:end]; // create date from string
    
    NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
    [dateFormatter2 setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    [dateFormatter2 setTimeZone:[NSTimeZone localTimeZone]];
    dateFormatter2.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    
    NSString *currentTime = [dateFormatter2 stringFromDate:[NSDate date]];
    
    NSDate *now = [dateFormatter2 dateFromString:currentTime]; // create date from string
    
    NSTimeInterval distanceBetweenDates = [sDate timeIntervalSinceDate:now];
  
    NSInteger min = distanceBetweenDates/60;
    
    return [NSString stringWithFormat:@"%ld",(long)min];
}
@end

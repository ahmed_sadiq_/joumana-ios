//
//  InternalTableView.m
//  Joumana
//
//  Created by Ahmed Sadiq on 22/12/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "InternalTableView.h"
#import "SubInternalVC.h"
#import "PostCell.h"
#import "PostModel.h"
@interface InternalTableView ()

@end

@implementation InternalTableView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    if(_option == 1) {
        _titleLbl.text = @"القراءات اليومية";
    }
    else if(_option == 2) {
        _titleLbl.text = @"القراءات الأسبوعية";
    }
    else if(_option == 3) {
        _titleLbl.text = @"أرقام الحظ";
    }
    else if(_option == 10){
        _titleLbl.text = @"الأبراج اليومية";
        
    }   else if(_option == 20){
        _titleLbl.text = @"التوقعات السنوية";
        
    }
    else {
        _titleLbl.text = @"نصيحة";
    }
    
    if(_postArray.count == 0) {
        _noDataLbl.hidden = false;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return _postArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *simpleTableIdentifier = @"PostCell";
    PostCell *cell = (PostCell *)[_postTblView dequeueReusableCellWithIdentifier:nil];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PostCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    PostModel *pModel = [self.postArray objectAtIndex:indexPath.row];
    
    cell.lbl.text = pModel.desc;
    cell.lblTitle.text = pModel.title;
    cell.imgView.imageURL = [NSURL URLWithString:pModel.thumbnail];
    NSURL *url = [NSURL URLWithString:pModel.thumbnail];
    [[AsyncImageLoader sharedLoader] loadImageWithURL:url];
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    PostModel *pModel = [self.postArray objectAtIndex:indexPath.row];
    
    SubInternalVC *starViewCont = [[SubInternalVC alloc] initWithNibName:@"SubInternalVC" bundle:nil];
    starViewCont.pModel = pModel;
    starViewCont.option = _option;
    [self.navigationController pushViewController:starViewCont animated:YES];
    [self.navigationController setNavigationBarHidden:YES];
    
}

@end

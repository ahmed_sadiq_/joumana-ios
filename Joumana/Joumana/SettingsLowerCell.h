//
//  SettingsLowerCell.h
//  Joumana
//
//  Created by Ahmed Sadiq on 26/09/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsLowerCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIView *view2;
@property (strong, nonatomic) IBOutlet UIView *remTimeView;
@property (strong, nonatomic) IBOutlet UILabel *minRemLbl;
@property (strong, nonatomic) IBOutlet UILabel *titleLbl;
@property (strong, nonatomic) IBOutlet UILabel *dateLbl;
@property (strong, nonatomic) IBOutlet UILabel *timeLbl;
@property (strong, nonatomic) IBOutlet UILabel *dateLblView2;
@property (strong, nonatomic) IBOutlet UILabel *timeLblView2;
@end

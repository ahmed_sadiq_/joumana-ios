//
//  CallerScreenVC.h
//  Joumana
//
//  Created by Ahmed Sadiq on 07/11/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import <TwilioSDK/TwilioClient.h>
#import "BookedCall.h"
@interface CallerScreenVC : UIViewController<TCDeviceDelegate,TCConnectionDelegate> {
    AppDelegate *delegate;
    BOOL isEndCall;
    IBOutlet UILabel *callingLbl;
    NSTimer *timerTask;
    NSTimer *remainingTimerTask;
}

@property int timeDuration;
@property (strong, nonatomic) BookedCall *bCall;
@property (strong, nonatomic) IBOutlet UILabel *TitleLbl;
@property (strong, nonatomic) IBOutlet UIButton *callBtn;
@property (nonatomic, strong) TCDevice *device;
@property (nonatomic, strong) TCConnection *connection;

- (IBAction)callBtnPressed:(id)sender;
@end

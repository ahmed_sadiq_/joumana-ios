//
//  SignUpVC.h
//  Joumana
//
//  Created by Ahmed Sadiq on 26/09/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

#import "NIDropDown.h"


@interface SignUpVC : UIViewController<UITextFieldDelegate,UIGestureRecognizerDelegate,NIDropDownDelegate> {
    NSString *emailStr;
    NSString *name;
    NSString *profilePicUrl;
    NSString *gender;
    BOOL isLoginFromNumber;
    
    UITapGestureRecognizer *tap ;
    
    NIDropDown *dropDown;
}
@property (strong, nonatomic) IBOutlet UIButton *optionBtn;

@property (strong, nonatomic) IBOutlet UIView *holderView;
@property (strong, nonatomic) IBOutlet UITextField *fullName;
@property (strong, nonatomic) IBOutlet UITextField *bdayTxt;
@property (strong, nonatomic) IBOutlet UITextField *email;
@property (strong, nonatomic) IBOutlet UITextField *pswd;

@property (strong, nonatomic) IBOutlet UIView *dobView;
@property (strong, nonatomic) IBOutlet UIDatePicker *dobPicker;

- (IBAction)dobValueChanged:(id)sender;

- (IBAction)signUpPressed:(id)sender;
- (IBAction)backToLogin:(id)sender;
- (IBAction)birthdayPressed:(id)sender;
- (IBAction)dobDone:(id)sender;
- (IBAction)fbPressed:(id)sender;
@end

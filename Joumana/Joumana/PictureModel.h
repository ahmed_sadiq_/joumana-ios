//
//  PictureModel.h
//  Joumana
//
//  Created by Ahmed Sadiq on 22/12/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseEntity.h"
@interface PictureModel : BaseEntity
@property (nonatomic, strong) NSString * picture;
- (id)initWithDictionary:(NSDictionary *) responseData;
@end

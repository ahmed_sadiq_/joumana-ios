//
//  Videos.m
//  Joumana
//
//  Created by Ahmed Sadiq on 18/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "Videos.h"

@implementation Videos

- (id)initWithDictionary:(NSDictionary *) responseData
{
    
    self = [super init];
    
    if (self) {
        
        self.date_time = [self validStringForObject:responseData [@"date_time"]];
        self.video  = [self completeImageURL:responseData [@"video"]];
        self.video_id  = [self validStringForObject:responseData [@"video_id"]];
        self.thumbnail  = [self validStringForObject:responseData [@"thumbnail"]];
        
    }
    
    return self;
}

@end

//
//  CallerScreenVC.m
//  Joumana
//
//  Created by Ahmed Sadiq on 07/11/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "CallerScreenVC.h"
#import "AppDelegate.h"

#define TOKEN_URL @"https://joumana.witsapplication.com/Token/token"


@interface CallerScreenVC ()

@end

@implementation CallerScreenVC
@synthesize timeDuration,bCall;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    delegate = (AppDelegate*) [UIApplication sharedApplication].delegate;
    [self retrieveToken];
    _TitleLbl.text = delegate.sharedUserModel.name;
    timeDuration = timeDuration*60;
    
    remainingTimerTask = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(remainingTick:) userInfo:nil repeats:YES];
    
}

- (void) remainingTick :(NSTimer*) timer {
    timeDuration--;
    
    if(timeDuration <= 0) {
        [remainingTimerTask invalidate];
        remainingTimerTask = nil;
        
        [timerTask invalidate];
        timerTask = nil;
        
        callingLbl.text = @"Time is Over";
        callingLbl.hidden = false;
        self.callBtn.enabled = false;
    }
}
- (IBAction)backPressed:(id)sender {
    if (self.connection) {
        [self.connection disconnect];
        callingLbl.hidden = true;
        isEndCall = false;
        [self.callBtn setImage:[UIImage imageNamed:@"callBtn"] forState:UIControlStateNormal];
        self.callBtn.enabled = true;
        
    }
    AppDelegate *appDel = (AppDelegate*) [UIApplication sharedApplication].delegate;
    [appDel.sharedUserModel.bookedCallsArray removeObject:self.bCall];
    [self.navigationController popViewControllerAnimated:true];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)callBtnPressed:(id)sender {
    if(isEndCall) {
        if (self.connection) {
            [self.connection disconnect];
            callingLbl.hidden = true;
            isEndCall = false;
            [self.callBtn setImage:[UIImage imageNamed:@"callBtn"] forState:UIControlStateNormal];
            self.callBtn.enabled = true;
            AppDelegate *appDel = (AppDelegate*) [UIApplication sharedApplication].delegate;
            [appDel.sharedUserModel.bookedCallsArray removeObject:self.bCall];
            [self.navigationController popViewControllerAnimated:true];
        }
    }
    else {
        if (self.device) {
            callingLbl.hidden = false;
            [self.device connect:@{@"To":@"+923454313430"} delegate:self];
            self.callBtn.enabled = NO;
        }
    }
    
}

#pragma mark Utility Methods
- (void) displayError:(NSString*)errorMessage {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Error" message:errorMessage preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark Initialization methods
- (void) initializeTwilioDevice:(NSString*)token {
    if(timeDuration > 10) {
        self.device = [[TCDevice alloc] initWithCapabilityToken:token delegate:self];
        self.callBtn.enabled = true;
        
        callingLbl.hidden = true;
        callingLbl.text = @"Calling...";
    }
    
}


- (void) retrieveToken {
    callingLbl.text = @"Configuring...";
    callingLbl.hidden = false;
    // Create a GET request to the capability token endpoint
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURL *url = [NSURL URLWithString:TOKEN_URL];
    NSURLRequest *request = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30.0];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable responseData, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (responseData) {
            NSError *error = nil;
            NSDictionary *responseObject = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:&error];
            if (responseObject) {
                if (responseObject[@"identity"]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        //self.navigationItem.title = responseObject[@"identity"];
                        
                        NSLog(@"%@", responseObject[@"identity"]);
                        
                    });
                }
                if (responseObject[@"token"]) {
                    NSLog(@"%@", responseObject[@"token"]);
                    [self initializeTwilioDevice:responseObject[@"token"]];
                }
            } else {
                [self displayError:[error localizedDescription]];
            }
        } else {
            [self displayError:[error localizedDescription]];
        }
    }];
    [task resume];
}


#pragma mark TCDeviceDelegate

- (void)device:(TCDevice *)device didStopListeningForIncomingConnections:(NSError *)error {
    if (error) {
        NSLog(@"%@",[error localizedDescription]);
    }
}

- (void)deviceDidStartListeningForIncomingConnections:(TCDevice *)device {
    //self.statusLabel.text = @"Started listening for incoming connections";
    
    NSLog(@"Started listening for incoming connections");
}

- (void)device:(TCDevice *)device didReceiveIncomingConnection:(TCConnection *)connection {
    if (connection.parameters) {
        NSString *from = connection.parameters[@"From"];
        NSString *message = [NSString stringWithFormat:@"Incoming call from %@",from];
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Incoming Call" message:message preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *acceptAction = [UIAlertAction actionWithTitle:@"Accept" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            connection.delegate = self;
            [connection accept];
            self.connection = connection;
        }];
        UIAlertAction *declineAction = [UIAlertAction actionWithTitle:@"Decline" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [connection reject];
        }];
        
        [alertController addAction:acceptAction];
        [alertController addAction:declineAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }
}



#pragma mark TCConnectionDelegate
- (void)connection:(TCConnection *)connection didFailWithError:(NSError *)error {
    NSLog(@"%@",[error localizedDescription]);
    isEndCall = false;
    callingLbl.hidden = true;
    
    [timerTask invalidate];
    timerTask = nil;
    
    [self.callBtn setImage:[UIImage imageNamed:@"callBtn"] forState:UIControlStateNormal];
}

- (void)connectionDidStartConnecting:(TCConnection *)connection {
    NSLog(@"Started connecting....");
}

- (void)connectionDidConnect:(TCConnection *)connection {
    self.connection = connection;
    NSLog(@"Connected");
    callingLbl.hidden = true;
    [self.callBtn setImage:[UIImage imageNamed:@"endCall"] forState:UIControlStateNormal];
    self.callBtn.enabled = true;
    isEndCall = true;
    
    timerTask = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(tick:) userInfo:nil repeats:NO];
}

- (void)connectionDidDisconnect:(TCConnection *)connection {
    
    [timerTask invalidate];
    timerTask = nil;
    
    
    callingLbl.hidden = true;
    NSLog(@"Disconnected");
    isEndCall = false;
    [self.callBtn setImage:[UIImage imageNamed:@"callBtn"] forState:UIControlStateNormal];
    self.callBtn.enabled = true;
}


- (void) tick:(NSTimer *) timer {
    //do something here..
    if(timeDuration<=0) {
        if (self.connection) {
            [self.connection disconnect];
            callingLbl.hidden = true;
            isEndCall = false;
            [self.callBtn setImage:[UIImage imageNamed:@"callBtn"] forState:UIControlStateNormal];
            self.callBtn.enabled = true;
            AppDelegate *appDel = (AppDelegate*) [UIApplication sharedApplication].delegate;
            [appDel.sharedUserModel.bookedCallsArray removeObject:self.bCall];
            [self.navigationController popViewControllerAnimated:true];
        }
    }
}
@end

//
//  CreditCard.h
//  Joumana
//
//  Created by Ahmed Sadiq on 24/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseEntity.h"

@interface CreditCard : BaseEntity

@property (nonatomic, strong) NSString * credit_card_id;
@property (nonatomic, strong) NSString * card_number;
@property (nonatomic, strong) NSString * expiry_month;
@property (nonatomic, strong) NSString * expiry_year;
@property (nonatomic, strong) NSString * cvc;
@property (nonatomic, strong) NSString * user_id;

- (id)initWithDictionary:(NSDictionary *) responseData;
@end

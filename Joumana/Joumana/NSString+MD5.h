//
//  NSString+MD5.h
//  Joumana
//
//  Created by Osama on 23/11/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (MD5)
- (NSString *)MD5String;
@end

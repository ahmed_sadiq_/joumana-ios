//
//  AudioModel.h
//  Joumana
//
//  Created by Ahmed Sadiq on 22/12/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseEntity.h"

@interface AudioModel : BaseEntity
@property (nonatomic, strong) NSString * audio;
- (id)initWithDictionary:(NSDictionary *) responseData;
@end

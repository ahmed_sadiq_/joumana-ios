//
//  DailyReadingVC.h
//  Joumana
//
//  Created by Ahmed Sadiq on 23/09/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface DailyReadingVC : UIViewController {
    AppDelegate *delegate;
}

- (IBAction)starPressed:(id)sender;
@end

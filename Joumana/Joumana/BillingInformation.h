//
//  BillingInformation.h
//  Joumana
//
//  Created by Ahmed Sadiq on 24/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface BillingInformation : UIViewController<UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UIButton *submitBtn;
@property (strong, nonatomic) IBOutlet UITextField *cardNum;
@property (strong, nonatomic) IBOutlet UITextField *expiryNum;
@property (strong, nonatomic) IBOutlet UITextField *cvcNum;
@property (strong, nonatomic) IBOutlet UIView *expiryView;
@property (strong, nonatomic) IBOutlet UIDatePicker *expiryPicker;

- (IBAction)submitPressed:(id)sender;
- (IBAction)backToLoginPressed:(id)sender;
- (IBAction)expiryDatePressed:(id)sender;
- (IBAction)expiryDonePressed:(id)sender;
@end

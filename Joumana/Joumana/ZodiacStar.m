//
//  ZodiacStar.m
//  Joumana
//
//  Created by Ahmed Sadiq on 18/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "ZodiacStar.h"
#import "Horoscope.h"
@implementation ZodiacStar

- (id)initWithDictionary:(NSDictionary *) responseData
{
    
    self = [super init];
    
    if (self) {
        
        self.starName = [self validStringForObject:responseData [@"name"]];
        self.starId = [self validStringForObject:responseData [@"star_id"]];
        self.horoscopeArray = [[NSMutableArray alloc] init];
        self.isLikedBefore  = [self validStringForObject:responseData[@"is_liked_by_me"]];
        NSArray *hArray = (NSArray*)responseData [@"horoscope"];
        
        for(int i=0; i<hArray.count; i++) {
            NSDictionary *dictTemp = (NSDictionary*)[hArray objectAtIndex:i];
            Horoscope *horoscopeObj = [[Horoscope alloc] initWithDictionary:dictTemp];
            
            [self.horoscopeArray addObject:horoscopeObj];
        }
        
        
        
    }
    
    return self;
}

@end

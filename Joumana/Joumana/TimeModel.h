//
//  TimeModel.h
//  Joumana
//
//  Created by Osama on 23/11/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseEntity.h"
@interface TimeModel : BaseEntity
@property (strong, nonatomic) NSString *startTime;
@property (strong, nonatomic) NSString *endTime;
- (id)initWithDictionary:(NSDictionary *) responseData;
@end

//
//  NewsVC.h
//  Joumana
//
//  Created by Ahmed Sadiq on 21/09/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "NIDropDown.h"
#import "AsyncImageView.h"

@interface NewsVC : UIViewController <UITableViewDelegate,UITableViewDataSource,NIDropDownDelegate> {
    AppDelegate *delegate;
    NIDropDown *dropDown;
    NSArray * arr;
    NSString *selectedNewsId;
    NSMutableData *_responseData;
    NSMutableArray *favouriteArray;
}

@property (strong, nonatomic) IBOutlet UIPageControl *pager;
@property (strong, nonatomic) IBOutlet UIScrollView *upperScrollView;
@property (strong, nonatomic) IBOutlet UITableView *eventTblView;

@property (strong, nonatomic) IBOutlet UIButton *latestBtn;
@property (strong, nonatomic) IBOutlet UIButton *favoriteBtn;
@property (strong, nonatomic) IBOutlet UIButton *archiveBtn;
@property (strong, nonatomic) IBOutlet UIView *eventView;
@property (strong, nonatomic) IBOutlet AsyncImageView *detailImgView;
@property (strong, nonatomic) IBOutlet UILabel *detailTitleLbl;
@property (strong, nonatomic) IBOutlet UITextView *detailTxt;
@property (strong, nonatomic) IBOutlet UIButton *startBtn;



- (IBAction)detailStarPressed:(id)sender;
- (IBAction)latestPressed:(id)sender;
- (IBAction)favoritePressed:(id)sender;
- (IBAction)archivePressed:(id)sender;
- (IBAction)eventDonePressed:(id)sender;
- (IBAction)monthBtn:(id)sender;

- (IBAction)detailBackPressed:(id)sender;


- (IBAction)readMorePressed:(id)sender;

@property (strong, nonatomic) IBOutlet UIView *detailView;

- (IBAction)detailDonePressed:(id)sender;
@end

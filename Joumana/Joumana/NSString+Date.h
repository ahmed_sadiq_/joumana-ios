//
//  NSString+Date.h
//  Maggie
//
//  Created by Samreen on 01/05/2017.
//  Copyright © 2017 Hannan Khan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Date)
-(NSString *) convertDateINtoLocalTimezone:(NSString *)dateStr;
-(NSString *) getCallTimeWithStartTime:(NSString *)start andEndTime:(NSString *)end;
-(NSString *) getRemaningTimeWithCallStartTime:(NSString *)start;
-(NSString *) getCalldurationWithEndTime:(NSString *)end;
-(NSString *) convertDateINtoLocalTimezoneWithFormate:(NSString *)formate andDate:(NSString *)dateStr;
@end

//
//  BookedCall.h
//  Joumana
//
//  Created by Ahmed Sadiq on 18/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseEntity.h"

@interface BookedCall : BaseEntity

@property (nonatomic, strong) NSString * booked_call_id;
@property (nonatomic, strong) NSString * date_time;
@property (nonatomic, strong) NSString * call_time;
@property (nonatomic, strong) NSString * duration;
@property (nonatomic, strong) NSString * end_time;
@property (nonatomic, strong) NSString * start_time;
@property (nonatomic, strong) NSString * rem_time;
@property (nonatomic, strong) NSString * user_id;

- (id)initWithDictionary:(NSDictionary *) responseData;

@end

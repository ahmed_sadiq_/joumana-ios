//
//  PostModel.m
//  Joumana
//
//  Created by Ahmed Sadiq on 22/12/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "PostModel.h"
#import "PictureModel.h"
#import "VideoModel.h"
#import "AudioModel.h"
@implementation PostModel

- (id)initWithDictionary:(NSDictionary *) responseData
{
    
    self = [super init];
    
    if (self) {
        
        self.post_id = [self validStringForObject:responseData [@"post_id"]];
        self.title  = [self validStringForObject:responseData [@"title"]];
        self.type_id  = [self validStringForObject:responseData [@"type_id"]];
        self.star_id = [self validStringForObject:responseData [@"star_id"]];
        self.date  = [self validStringForObject:responseData [@"date"]];
        self.time  = [self validStringForObject:responseData [@"time"]];
        self.thumbnail  = [self validStringForObject:responseData [@"thumbnail"]];
        self.desc  = [self validStringForObject:responseData [@"description"]];
        
        NSMutableArray *pictures = [responseData objectForKey:@"pictures"];
        self.pictures = [[NSMutableArray alloc] init];
        for(int i=0; i<pictures.count; i++) {
            [self.pictures addObject: [[PictureModel alloc] initWithDictionary:[pictures objectAtIndex:i]]];
        }
        
        NSMutableArray *videos = [responseData objectForKey:@"videos"];
        self.videos = [[NSMutableArray alloc] init];
        for(int i=0; i<videos.count; i++) {
            [self.videos addObject: [[VideoModel alloc] initWithDictionary:[videos objectAtIndex:i]]];
        }
        
        NSMutableArray *audios = [responseData objectForKey:@"audios"];
        self.audio = [[NSMutableArray alloc] init];
        for(int i=0; i<audios.count; i++) {
            [self.audio addObject: [[AudioModel alloc] initWithDictionary:[audios objectAtIndex:i]]];
        }
        
    }
    
    return self;
}

@end

//
//  WorkingHours.h
//  Joumana
//
//  Created by Ahmed Sadiq on 18/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseEntity.h"
#import "TimeModel.h"
@interface WorkingHours : BaseEntity

@property (nonatomic, strong) NSString * day;
@property (nonatomic, strong) NSString * end_time;
@property (nonatomic, strong) NSString * start_time;
@property (nonatomic, strong) NSMutableArray *timesArray;
- (id)initWithDictionary:(NSDictionary *) responseData;

@end

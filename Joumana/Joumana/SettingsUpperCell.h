//
//  SettingsUpperCell.h
//  Joumana
//
//  Created by Ahmed Sadiq on 22/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsUpperCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *titleLbl;
@property (strong, nonatomic) IBOutlet UIImageView *sideIcon;
@end

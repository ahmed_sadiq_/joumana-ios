//
//  BookedCall.m
//  Joumana
//
//  Created by Ahmed Sadiq on 18/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "BookedCall.h"
#import "NSString+Date.h"

@implementation BookedCall

- (id)initWithDictionary:(NSDictionary *) responseData
{
    
    self = [super init];
    
    if (self) {
        NSString *startTime =[self validStringForObject:responseData [@"start_time"]];
        NSString *endTime =[self validStringForObject:responseData [@"end_time"]];
        NSString *dateTime = [self validStringForObject:responseData [@"date_time"]];

        
        self.booked_call_id = [self validStringForObject:responseData [@"booked_call_id"]];
        
        self.date_time = [[NSString alloc]convertDateINtoLocalTimezone:dateTime];
        self.duration = [self validStringForObject:responseData [@"duration"]];
        self.end_time = [[NSString alloc]convertDateINtoLocalTimezone:endTime];
        self.start_time  = [[NSString alloc]convertDateINtoLocalTimezone:startTime];
        self.user_id  = [self validStringForObject:responseData [@"user_id"]];
        self.call_time  =[[NSString alloc]getCallTimeWithStartTime:[self validStringForObject:responseData [@"start_time"]] andEndTime:[self validStringForObject:responseData [@"end_time"]]];

        self.rem_time  = [self validStringForObject:responseData [@"remaining_time"]];
       
    
    }
    
    return self;
}

@end

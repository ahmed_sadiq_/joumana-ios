//
//  TimeModel.m
//  Joumana
//
//  Created by Osama on 23/11/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "TimeModel.h"

@implementation TimeModel
- (id)initWithDictionary:(NSDictionary *) responseData
{
    
    self = [super init];
    
    if (self) {
        self.startTime  = [self validStringForObject:responseData [@"end_time"]];
        self.endTime  = [self validStringForObject:responseData [@"start_time"]];
        
    }
    return self;
}
@end

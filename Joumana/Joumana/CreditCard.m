//
//  CreditCard.m
//  Joumana
//
//  Created by Ahmed Sadiq on 24/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "CreditCard.h"

@implementation CreditCard

- (id)initWithDictionary:(NSDictionary *) responseData
{
    
    self = [super init];
    
    if (self) {
        
        self.credit_card_id = [self validStringForObject:responseData [@"credit_card_id"]];
        self.card_number = [self validStringForObject:responseData [@"card_number"]];
        self.expiry_month = [self validStringForObject:responseData [@"expiry_month"]];
        self.expiry_year = [self validStringForObject:responseData [@"expiry_year"]];
        self.cvc  = [self validStringForObject:responseData [@"cvc"]];
        self.user_id  = [self validStringForObject:responseData [@"user_id"]];
        
    }
    
    return self;
}
@end

//
//  Pictures.h
//  Joumana
//
//  Created by Ahmed Sadiq on 18/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseEntity.h"

@interface Pictures : BaseEntity

@property (nonatomic, strong) NSString * date_time;
@property (nonatomic, strong) NSString * picture;
@property (nonatomic, strong) NSString * picture_id;

- (id)initWithDictionary:(NSDictionary *) responseData;

@end

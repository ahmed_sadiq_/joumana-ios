//
//  BookCallVC.m
//  Joumana
//
//  Created by Ahmed Sadiq on 22/09/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "BookCallVC.h"
#import "DSLCalendarView.h"
#import "WorkingHours.h"
#import "CustomLoading.h"
#import "Utils.h"
#import "BookedCall.h"
#import <QuartzCore/QuartzCore.h>
#import "NSString+MD5.h"
#import "NSString+URLEncode.h"
@interface BookCallVC () <DSLCalendarViewDelegate>
@property (nonatomic, weak) IBOutlet UIView *calendarView;
@end

@implementation BookCallVC{
    NSString *callbackurl;
    NSString *bookCallID;
    NSString  *paymentAll;
    NSString *responseCode;
    BOOL isFromBack;

}

- (void)viewDidLoad {
    [super viewDidLoad];
    delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    _timePicker.hidden = YES;
    // Do any additional setup after loading the view from its nib.
    self.gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    callbackurl = @"https://joumana.witsapplication.com/joumana_payment.php?action=py";
    
    // border
    [self.calendarView.layer setBorderColor:[UIColor colorWithRed:0.816 green:(0.658) blue:(0.384) alpha:1.0].CGColor];
    [self.calendarView.layer setBorderWidth:1.5f];
    self.calendarView.layer.masksToBounds = true;
    
    self.dateFormatter = [[NSDateFormatter alloc] init];
    self.dateFormatter.dateFormat = @"yyyy-MM-dd";
    
    
    // 450 for iPad and 300 for iPhone
    CGFloat height = self.calendarView.frame.size.height;
    FSCalendar *calendar = [[FSCalendar alloc] initWithFrame:CGRectMake(0, 0, self.calendarView.frame.size.width, height)];
    calendar.dataSource = self;
    calendar.delegate = self;
    calendar.backgroundColor = [UIColor clearColor];
    calendar.appearance.headerMinimumDissolvedAlpha = 0;
    
    calendar.appearance.caseOptions = FSCalendarCaseOptionsHeaderUsesUpperCase;
    [self.calendarView addSubview:calendar];
    self.calendar = calendar;
    
    UIButton *previousButton = [UIButton buttonWithType:UIButtonTypeCustom];
    previousButton.frame = CGRectMake(-20, 5, 95, 34);
    previousButton.backgroundColor = [UIColor clearColor];
    previousButton.titleLabel.font = [UIFont systemFontOfSize:15];
    [previousButton setImage:[UIImage imageNamed:@"icon_prev"] forState:UIControlStateNormal];
    [previousButton addTarget:self action:@selector(previousClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.calendarView addSubview:previousButton];
    self.previousButton = previousButton;
    
    UIButton *nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
    nextButton.frame = CGRectMake(CGRectGetWidth(self.calendarView.frame)-75, 5, 95, 34);
    nextButton.backgroundColor = [UIColor clearColor];
    nextButton.titleLabel.font = [UIFont systemFontOfSize:15];
    [nextButton setImage:[UIImage imageNamed:@"icon_next"] forState:UIControlStateNormal];
    [nextButton addTarget:self action:@selector(nextClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.calendarView addSubview:nextButton];
    self.nextButton = nextButton;
    
    
    [self getCurrentWeekDay:[NSDate date]];
    [self setAvailableTimeForDay];
    
}
#pragma mark PICKER VIEW DELEGATES
- (void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component {
    // Handle the selection
}

// tell the picker how many rows are available for a given component
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    WorkingHours *wHours = [delegate.sharedUserModel.workingHoursArray objectAtIndex:weekdayInt - 2];
    return wHours.timesArray.count;
}

// tell the picker how many components it will have
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

// tell the picker the title for a given component
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    NSString *title;
    WorkingHours *wHours = [delegate.sharedUserModel.workingHoursArray objectAtIndex:weekdayInt - 2];
    
    title = [wHours.timesArray objectAtIndex:row];
    
    return title;
}

// tell the picker the width of each row for a given component
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    int sectionWidth = 237;
    
    return sectionWidth;
}

- (void)setAvailableTimeForDay {
    //    WorkingHours *wHours = [delegate.sharedUserModel.workingHoursArray objectAtIndex:weekdayInt-1];
    //
    //    NSString *startTimeTemp = wHours.start_time;
    //    NSString *endTimeTemp = wHours.end_time;
    //
    //    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    //    [formatter setDateFormat:@"HH:mm:ss"];
    //    NSDate *startdateTemp = [formatter dateFromString:startTimeTemp];
    //
    //    NSDateFormatter *formatter1 = [[NSDateFormatter alloc]init];
    //    [formatter1 setDateFormat:@"HH:mm:ss"];
    //    NSDate *enddateTemp = [formatter dateFromString:endTimeTemp];
    //
    //    _hoursPicker.minimumDate = startdateTemp;
    //    _hoursPicker.maximumDate = enddateTemp;
    //
    //    [_hoursPicker setDate:startdateTemp];
    
}

- (void) getCurrentWeekDay : (NSDate *) dateToBeSeen {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"EEEE"];
    NSLog(@"The day of the week: %@", [dateFormatter stringFromDate:dateToBeSeen]);
    NSDateFormatter* df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd"];
    NSString *formattedString = [df stringFromDate:dateToBeSeen];
    NSDate* enteredDate = [df dateFromString:formattedString];
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *comps = [gregorian components:NSCalendarUnitWeekday fromDate:dateToBeSeen];
   
    NSString *dateString = [df stringFromDate:[NSDate date]];
    NSDate* today = [df dateFromString:dateString];
    NSComparisonResult result = [today compare:enteredDate];
    switch (result)
    {
        case NSOrderedAscending:
            weekdayInt = (int)[comps weekday];
            if(weekdayInt == 1)
                weekdayInt = 6;
            [self.timePickerP reloadAllComponents];
            break;
        case NSOrderedDescending:
            [self presentViewController:[Utils showCustomAlert:@"" andMessage:@"Earlier Date is Selected"] animated:YES completion:nil];
            selectedDate = @"";
            break;
        case NSOrderedSame:
            NSLog(@"Today/Null Date Passed");
            weekdayInt = (int)[comps weekday];
            if(weekdayInt == 1)
                weekdayInt = 6;
            [self.timePickerP reloadAllComponents];//Not sure why This is case when null/wrong date is passed
            break;
    }

//    weekdayInt = (int)[comps weekday];
//    if(weekdayInt == 1)
//        weekdayInt = 6;
//    [self.timePickerP reloadAllComponents];
}

- (void)previousClicked:(id)sender
{
    NSDate *currentMonth = self.calendar.currentPage;
    NSDate *previousMonth = [self.gregorian dateByAddingUnit:NSCalendarUnitMonth value:-1 toDate:currentMonth options:0];
    [self.calendar setCurrentPage:previousMonth animated:YES];
}

- (void)nextClicked:(id)sender
{
    NSDate *currentMonth = self.calendar.currentPage;
    NSDate *nextMonth = [self.gregorian dateByAddingUnit:NSCalendarUnitMonth value:1 toDate:currentMonth options:0];
    [self.calendar setCurrentPage:nextMonth animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - DSLCalendarViewDelegate methods

- (void)calendarView:(DSLCalendarView *)calendarView didSelectRange:(DSLCalendarRange *)range {
    if (range != nil) {
        NSLog( @"Selected %ld/%ld - %ld/%ld", (long)range.startDay.day, (long)range.startDay.month, (long)range.endDay.day, (long)range.endDay.month);
    }
    else {
        NSLog( @"No selection" );
    }
}

- (DSLCalendarRange*)calendarView:(DSLCalendarView *)calendarView didDragToDay:(NSDateComponents *)day selectingRange:(DSLCalendarRange *)range {
    if (NO) { // Only select a single day
        return [[DSLCalendarRange alloc] initWithStartDay:day endDay:day];
    }
    else if (YES) { // Don't allow selections before today
        NSDateComponents *today = [[NSDate date] dslCalendarView_dayWithCalendar:calendarView.visibleMonth.calendar];
        
        NSDateComponents *startDate = range.startDay;
        NSDateComponents *endDate = range.endDay;
        
        if ([self day:startDate isBeforeDay:today] && [self day:endDate isBeforeDay:today]) {
            return nil;
        }
        else {
            if ([self day:startDate isBeforeDay:today]) {
                startDate = [today copy];
            }
            if ([self day:endDate isBeforeDay:today]) {
                endDate = [today copy];
            }
            
            return [[DSLCalendarRange alloc] initWithStartDay:startDate endDay:endDate];
        }
    }
    
    return range;
}

- (void)calendarView:(DSLCalendarView *)calendarView willChangeToVisibleMonth:(NSDateComponents *)month duration:(NSTimeInterval)duration {
    NSLog(@"Will show %@ in %.3f seconds", month, duration);
}

- (void)calendarView:(DSLCalendarView *)calendarView didChangeToVisibleMonth:(NSDateComponents *)month {
    NSLog(@"Now showing %@", month);
}

- (BOOL)day:(NSDateComponents*)day1 isBeforeDay:(NSDateComponents*)day2 {
    return ([day1.date compare:day2.date] == NSOrderedAscending);
}

#pragma mark - Drop Down methods

- (void) niDropDownDelegateMethod: (NIDropDown *) sender {
    
    minutesSelected = true;
    [self rel];
}

-(void)rel{
    //    [dropDown release];
    dropDown = nil;
}

- (IBAction)selectMinutesPressed:(id)sender {
    arr = [[NSArray alloc] init];
    arr = [NSArray arrayWithObjects:@"20",@"30",nil];
    NSArray * arrImage = nil;
    if(dropDown == nil) {
        CGFloat f = 180;
        dropDown = [[NIDropDown alloc]showDropDown:sender :&f :arr :arrImage :@"down"];
        dropDown.delegate = self;
    }
    else {
        [dropDown hideDropDown:sender];
        [self rel];
    }
}

- (IBAction)menuPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

- (IBAction)confirmPressed:(id)sender {
    if(minutesSelected && timeSelected) {
        [self bookCallWithServer];
    }
    else {
        [self presentViewController:[Utils showCustomAlert:@"خطأ التحقق من صحة" andMessage:@"يرجى تحديد الوقت الصحيح ومدة المكالمة"] animated:YES completion:nil];
    }
    //[self.navigationController popViewControllerAnimated:true];
}

- (void) bookCallWithServer {
    [CustomLoading showAlertMessage];
    
    NSString *urlStr = [NSString stringWithFormat:@"%@%@",BASE_URL,@"Call/book_call2"];
    NSURL *url = [NSURL URLWithString:urlStr];
    if([_durationBtn.titleLabel.text isEqualToString:@"10"]){
        paymentAll = @"200";
    }
    else if ([_durationBtn.titleLabel.text isEqualToString:@"20"]){
        paymentAll = @"350";
    }
    else{
        paymentAll = @"500";
    }
    
    NSMutableDictionary *postParams = [[NSMutableDictionary alloc] init];
    [postParams setObject:delegate.sharedUserModel.userID forKey:@"user_id"];
    [postParams setObject:_durationBtn.titleLabel.text forKey:@"duration"];
    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
      NSString *startDateTime = [NSString stringWithFormat:@"%@ %@:00",selectedDate,_availableTimeBtn.titleLabel.text];
    NSDate *sDate = [dateFormatter dateFromString:startDateTime];

    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];

  

    NSString *currentTime = [dateFormatter stringFromDate:sDate];

    [postParams setObject:currentTime forKey:@"start_time"];
    
    NSData *postData = [Utils encodeDictionary:postParams];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        
        [CustomLoading DismissAlertMessage];
        
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int flag = [[result objectForKey:@"status"] intValue];
            
            if(flag == 1) {
                
                NSDictionary *call_data = [result objectForKey:@"call_data"];
                BookedCall *bCall = [[BookedCall alloc] initWithDictionary:call_data];
                bookCallID = bCall.booked_call_id;
            
                [delegate.sharedUserModel.bookedCallsArray addObject:bCall];
                self.paymentMainView.hidden = NO;
                self.paymentWebView.hidden = NO;
                [self openWebView];
                
            }
            else {
                
                [self presentViewController:[Utils showCustomAlert:@"حدث خطأ ما" andMessage:[result objectForKey:@"message"]] animated:YES completion:nil];
                
            }
        }
        else{
            
            [self presentViewController:[Utils showCustomAlert:@"خطأ كونيكشن " andMessage:@"يرجى إعادة المحاولة بعد بعض الوقت"] animated:YES completion:nil];
        }
    }];
}
-(void)openWebView{
    NSString *merchant = @"222200548001";
    NSString *accessCode = @"C2D01CD4";
    int p = [paymentAll intValue];
    p *= 100;
 
    NSString *amount = [NSString stringWithFormat:@"%d",p];
    long m = arc4random() % 10000000000000;
    //NSString *merchTxnRef = [NSString stringWithFormat:@"%ld",m];

    long currentTime = (long)(NSTimeInterval)([[NSDate date] timeIntervalSince1970]);
    NSString *merchTxnRef = [NSString stringWithFormat:@"%ld",currentTime];

    long o = arc4random() % 10000000000000;
    NSString *orderInfo = [NSString stringWithFormat:@"%ld",currentTime]; //[[NSProcessInfo processInfo] globallyUniqueString];//[NSString stringWithFormat:@"%ld",o];
    NSString *params = [NSString stringWithFormat
                        :@"AF265AF4FF266FEA1D1BF73D0B299040%@%@%@%@%@%@",accessCode,amount,merchTxnRef,merchant,orderInfo,callbackurl];
    NSString *md5Hashed = [params MD5String];
    md5Hashed = [md5Hashed uppercaseString];
    NSString *finalUrl = [NSString stringWithFormat:@"https://gw1.audicards.com/TPGWeb/payment/prepayment.action?accessCode=%@&amount=%@&merchTxnRef=%@&merchant=%@&orderInfo=%@&returnURL=%@&vpc_SecureHash=%@",[accessCode URLEncodeUsingEncoding:NSUTF8StringEncoding],[amount URLEncodeUsingEncoding:NSUTF8StringEncoding],[merchTxnRef URLEncodeUsingEncoding:NSUTF8StringEncoding],[merchant URLEncodeUsingEncoding:NSUTF8StringEncoding],[orderInfo URLEncodeUsingEncoding:NSUTF8StringEncoding],[callbackurl URLEncodeUsingEncoding:NSUTF8StringEncoding],md5Hashed ];
    
    [self.paymentWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:finalUrl]]];
}
-(void)parseURl:(NSString *)urlString{
    NSMutableDictionary *queryStringDictionary = [[NSMutableDictionary alloc] init];
    NSArray *urlComponents = [urlString componentsSeparatedByString:@"&"];
    for (NSString *keyValuePair in urlComponents)
    {
        NSArray *pairComponents = [keyValuePair componentsSeparatedByString:@"="];
        NSString *key = [[pairComponents firstObject] stringByRemovingPercentEncoding];
        NSString *value = [[pairComponents lastObject] stringByRemovingPercentEncoding];
        [queryStringDictionary setObject:value forKey:key];
        
    }
   // int success = [[queryStringDictionary objectForKey:@"vpc_TxnResponseCode"] intValue];
    NSString *success =[self validStringForObject:[queryStringDictionary objectForKey:@"vpc_TxnResponseCode"]];

     [self.paymentWebView setHidden:YES];
    self.paymentMainView.hidden = YES;
    if([success isEqualToString:@"0"])
    {
        [self addPaymentToserver];
    }
    else{
        responseCode = [NSString stringWithFormat:@"%@",success];
        isFromBack = NO;
        [self deleteCallFromserver];
    }
    
   
    
}
-(void)addPaymentToserver{
    [CustomLoading showAlertMessage];
    
    NSString *urlStr = [NSString stringWithFormat:@"%@%@",BASE_URL,@"Transaction/add_payment"];
    NSURL *url = [NSURL URLWithString:urlStr];
    
    
    NSMutableDictionary *postParams = [[NSMutableDictionary alloc] init];
    [postParams setObject:bookCallID forKey:@"booked_call_id"];
    [postParams setObject:paymentAll forKey:@"payment"];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *date_time = [NSString stringWithFormat:@"%@", [dateFormatter stringFromDate: [NSDate date]]];
    
    [postParams setObject:date_time forKey:@"date_time"];
    NSData *postData = [Utils encodeDictionary:postParams];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        
        [CustomLoading DismissAlertMessage];
        
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int flag = [[result objectForKey:@"status"] intValue];
            
            if(flag == 1) {
                  [self presentViewController:[Utils showCustomAlert:@"" andMessage:[result objectForKey:@"message"]] animated:YES completion:nil];
            }
            else {
                
                [self presentViewController:[Utils showCustomAlert:@"" andMessage:[result objectForKey:@"message"]] animated:YES completion:nil];
                
            }
        }
        else{
            
            [self presentViewController:[Utils showCustomAlert:@"خطأ كونيكشن " andMessage:@"يرجى إعادة المحاولة بعد بعض الوقت"] animated:YES completion:nil];
        }
    }];

}


-(void)deleteCallFromserver{
    [CustomLoading showAlertMessage];
    
    NSString *urlStr = [NSString stringWithFormat:@"%@%@",BASE_URL,@"Call/delete_booked_call"];
    NSURL *url = [NSURL URLWithString:urlStr];
    
    
    NSMutableDictionary *postParams = [[NSMutableDictionary alloc] init];
    [postParams setObject:bookCallID forKey:@"booked_call_id"];
    [postParams setObject:responseCode forKey:@"responseCode"];

    NSData *postData = [Utils encodeDictionary:postParams];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        
        [CustomLoading DismissAlertMessage];
        
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int flag = [[result objectForKey:@"status"] intValue];
            
            if(flag == 1) {
                if (!isFromBack) {
              
                 [self presentViewController:[Utils showCustomAlert:@"" andMessage:[result objectForKey:@"message"]] animated:YES completion:nil];
                }
            }
            else {
                
               // [self presentViewController:[Utils showCustomAlert:@"" andMessage:[result objectForKey:@"message"]] animated:YES completion:nil];
                
            }
        }
        else{
            
            [self presentViewController:[Utils showCustomAlert:@"خطأ كونيكشن " andMessage:@"يرجى إعادة المحاولة بعد بعض الوقت"] animated:YES completion:nil];
        }
    }];
    
}

#pragma  mark WEBVIEW DELEGATES
- (BOOL)webView:(UIWebView*)webView shouldStartLoadWithRequest:(NSURLRequest*)request navigationType:(UIWebViewNavigationType)navigationType {
    //CAPTURE USER LINK-CLICK.
    
    NSString *urlStrin = request.URL.absoluteString;
    NSLog(@"Loading URL :%@",request.URL.absoluteString);
    if([urlStrin containsString:callbackurl]){
        [self parseURl:urlStrin];
        return NO;
    }
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [_actInd startAnimating];
    NSLog(@"did start loading");
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [_actInd stopAnimating];
    NSLog(@"did finish loading");
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [_actInd stopAnimating];
    NSLog(@"Failed to load with error :%@",[error debugDescription]);
}
- (IBAction)timePickerDonePressed:(id)sender {
    _timePicker.hidden = true;
    timeSelected = true;
    WorkingHours *wHours = [delegate.sharedUserModel.workingHoursArray objectAtIndex:weekdayInt - 2];
    NSString *selectedTime = [wHours.timesArray objectAtIndex:[self.timePickerP selectedRowInComponent:0]];
    //    NSDate *selectedTime = [_hoursPicker date];
    //    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    //    [formatter setDateFormat:@"HH:mm"];
    //
    //    NSString *selectedTimeStr = [formatter stringFromDate:selectedTime];
    
    [_availableTimeBtn setTitle:selectedTime forState:UIControlStateNormal];
    
}

- (IBAction)timeSelctionPressed:(id)sender {
    //    HSDatePickerViewController *hsdpvc = [[HSDatePickerViewController alloc] init];
    //    hsdpvc.delegate = self;
    //    hsdpvc.date = [NSDate date];
    //    [self presentViewController:hsdpvc animated:YES completion:nil];
    
    _timePicker.hidden = false;
    
}

#pragma mark - FSCalendarDelegate

- (void)calendar:(FSCalendar *)calendar didSelectDate:(NSDate *)date
{
    [self getCurrentWeekDay:date];
    
    selectedDate = [self.dateFormatter stringFromDate:date];
}

- (void)calendarCurrentPageDidChange:(FSCalendar *)calendar
{
    //NSLog(@"did change page %@",[self.dateFormatter stringFromDate:calendar.currentPage]);
}
- (IBAction)paymentMainViewBackPressed:(id)sender {
    
    self.paymentMainView.hidden = YES;
    isFromBack = YES;
    responseCode = @"";
    [self deleteCallFromserver];

}

- (NSString *) validStringForObject:(NSString *) object{
    
    if (![object isKindOfClass:[NSNull class]])
        return object;
    
    return @"";
    
}
@end

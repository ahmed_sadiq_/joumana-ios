//
//  DateCell.h
//  Joumana
//
//  Created by Ahmed Sadiq on 22/12/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DateCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *dateLbl;

@end

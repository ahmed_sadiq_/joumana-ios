//
//  ViewController.m
//  Joumana
//
//  Created by Ahmed Sadiq on 20/09/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "ViewController.h"
#import "ExploreVC.h"
#import "SignInVC.h"
#import "SignUpVC.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)exploreBtnPressed:(id)sender {
    ExploreVC *exploreVC = [[ExploreVC alloc] initWithNibName:@"ExploreVC" bundle:nil];
    [self.navigationController pushViewController:exploreVC animated:YES];
    //[self.navigationController setNavigationBarHidden:YES];
}
- (IBAction)signInPressed:(id)sender {
    SignInVC *signInVC = [[SignInVC alloc] initWithNibName:@"SignInVC" bundle:nil];
    [self.navigationController pushViewController:signInVC animated:YES];
    //[self.navigationController setNavigationBarHidden:YES];
}
- (IBAction)registerPressed:(id)sender {
    SignUpVC *signUpVC = [[SignUpVC alloc] initWithNibName:@"SignUpVC" bundle:nil];
    [self.navigationController pushViewController:signUpVC animated:YES];
}
- (IBAction)backPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

@end

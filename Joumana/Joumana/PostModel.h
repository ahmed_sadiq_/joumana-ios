//
//  PostModel.h
//  Joumana
//
//  Created by Ahmed Sadiq on 22/12/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseEntity.h"

@interface PostModel : BaseEntity

@property (nonatomic, strong) NSString * post_id;
@property (nonatomic, strong) NSString * title;
@property (nonatomic, strong) NSString * type_id;
@property (nonatomic, strong) NSString * star_id;
@property (nonatomic, strong) NSString * date;
@property (nonatomic, strong) NSString * time;
@property (nonatomic, strong) NSString * thumbnail;
@property (nonatomic, strong) NSString * desc;
@property (nonatomic, strong) NSMutableArray * pictures;
@property (nonatomic, strong) NSMutableArray * videos;
@property (nonatomic, strong) NSMutableArray * audio;


- (id)initWithDictionary:(NSDictionary *) responseData;

@end

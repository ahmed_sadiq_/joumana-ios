//
//  Events.m
//  Joumana
//
//  Created by Ahmed Sadiq on 18/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "Events.h"

@implementation Events

- (id)initWithDictionary:(NSDictionary *) responseData
{
    
    self = [super init];
    
    if (self) {
        
        self.date_time = [self validStringForObject:responseData [@"date_time"]];
        self.detail = [self validStringForObject:responseData [@"detail"]];
        self.event_id = [self validStringForObject:responseData [@"event_id"]];
        self.event_time = [self completeImageURL:responseData [@"event_time"]];
        self.picture  = [self completeImageURL:responseData [@"picture"]];
        self.title  = [self validStringForObject:responseData [@"title"]];
        
    }
    
    return self;
}
@end

//
//  ForgotPswdVC.m
//  Joumana
//
//  Created by Ahmed Sadiq on 26/09/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "ForgotPswdVC.h"
#import "Utils.h"
#import "CustomLoading.h"
#import "Constants.h"
@interface ForgotPswdVC ()

@end

@implementation ForgotPswdVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.email setValue:[UIColor whiteColor]
              forKeyPath:@"_placeholderLabel.textColor"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)backToPasswordPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}
- (BOOL) validateFields {
    if(![Utils validateEmail:_email.text]) {
        return false;
    }
    return true;
}
- (IBAction)resetPswdPressed:(id)sender {
    if([self validateFields]){
        [CustomLoading showAlertMessage];
        
        NSString *urlStr = [NSString stringWithFormat:@"%@%@",BASE_URL,
                            @"User/forgot_password"];
        NSURL *url = [NSURL URLWithString:urlStr];
        
        NSMutableDictionary *postParams = [[NSMutableDictionary alloc] init];
        [postParams setObject:_email.text forKey:@"email"];
        
        
        NSData *postData = [Utils encodeDictionary:postParams];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:url];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:postData];
        
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
            
            [CustomLoading DismissAlertMessage];
            
            if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
            {
                NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                int flag = [[result objectForKey:@"status"] intValue];
                
                if(flag == 1) {
                    [self presentViewController:[Utils showCustomAlert:@"" andMessage:[result objectForKey:@"message"]] animated:YES completion:nil];
                }else {
                    
                    [self presentViewController:[Utils showCustomAlert:@"حدث خطأ ما" andMessage:[result objectForKey:@"message"]] animated:YES completion:nil];
                    
                }
            }
            else{
                
                [self presentViewController:[Utils showCustomAlert:@"" andMessage:@"الرجاء التحقق من أتصال الأنترنت"] animated:YES completion:nil];
            }
        }];
    }
    else {
        [self presentViewController:[Utils showCustomAlert:@"" andMessage:@"الرجاء إدخال عنوان بريد إلكتروني صالح"] animated:YES completion:nil];
    }
    
}
- (IBAction)backPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}
@end

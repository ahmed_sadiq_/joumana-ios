//
//  AppDelegate.h
//  Joumana
//
//  Created by Ahmed Sadiq on 20/09/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"
#import "HomeVC.h"
#import "User.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property ( strong , nonatomic ) UINavigationController *navigationController;
@property ( strong , nonatomic ) ViewController *viewController;
@property ( strong , nonatomic ) HomeVC *homeVC;
@property ( strong , nonatomic ) User *sharedUserModel;
@property ( strong , nonatomic ) NSString *deviceToken;
@property ( strong , nonatomic ) NSString * subscriptionUrl;;

@end


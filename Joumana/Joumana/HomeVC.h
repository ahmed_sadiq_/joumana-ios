//
//  HomeVC.h
//  Joumana
//
//  Created by Ahmed Sadiq on 20/09/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <StoreKit/StoreKit.h>

@interface HomeVC : UIViewController<NSURLConnectionDelegate,SKPaymentTransactionObserver, SKProductsRequestDelegate>
{
    NSMutableData *_responseData;
    
    //IBOutlet UIView *subscriptionView;
    IBOutlet UILabel *subInnerView;
    
}
@property (weak, nonatomic) IBOutlet UIView *privacyView;
@property (weak, nonatomic) IBOutlet UIView *subscripeOptionView;
@property (strong, nonatomic) IBOutlet UIView *subscriptionView;
@property (strong, nonatomic) IBOutlet UIWebView *subWebView;
@property (strong, nonatomic) NSMutableArray *postArray;
@property (weak, nonatomic) IBOutlet UIButton *btnYearly;

- (IBAction)subBackPressed:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btngallery;


@property (strong, nonatomic) SKProduct *product;
@property (strong, nonatomic) NSString *productID;


- (IBAction)subscripeBtnPressed:(id)sender;
- (IBAction)cancelBtnPressed:(id)sender;
- (IBAction)subscriptionDetails:(id)sender;

- (IBAction)newspressed:(id)sender;


@property (strong, nonatomic) IBOutlet UIView *subscriptionHelpView;

- (IBAction)subscriptionCancelPressed:(id)sender;
- (IBAction)visitPrivacyPolicy:(id)sender;
- (IBAction)aboutSubscriptionPressed:(id)sender;


@end

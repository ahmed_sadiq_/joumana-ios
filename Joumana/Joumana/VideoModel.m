//
//  VideoModel.m
//  Joumana
//
//  Created by Ahmed Sadiq on 22/12/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "VideoModel.h"

@implementation VideoModel

- (id)initWithDictionary:(NSDictionary *) responseData
{
    
    self = [super init];
    
    if (self) {
        
        self.thumbnail = [self completeImageURL:responseData [@"thumbnail"]];
        self.video  = [self completeImageURL:responseData [@"video"]];
        
    }
    
    return self;
}

@end

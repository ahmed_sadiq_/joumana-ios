//
//  EditProfileVC.m
//  Joumana
//
//  Created by Ahmed Sadiq on 24/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "EditProfileVC.h"
#import "UIImageView+RoundImage.h"
#import "Utils.h"
#import "CustomLoading.h"
#import "AFNetworking.h"
@interface EditProfileVC ()

@end

@implementation EditProfileVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
    delegate = (AppDelegate*) [UIApplication sharedApplication].delegate;
    
    [self.submitBtn.layer setBorderColor:[UIColor colorWithRed:0.816 green:(0.658) blue:(0.384) alpha:1.0].CGColor];
    self.submitBtn.layer.borderWidth = 1.5f;
    self.submitBtn.layer.masksToBounds = true;
    
    
    
    [self.imgView.layer setBorderColor:[UIColor colorWithRed:0.816 green:(0.658) blue:(0.384) alpha:1.0].CGColor];
    self.imgView.layer.borderWidth = 2.0f;
    self.imgView.layer.masksToBounds = true;
    
    [_imgView roundImageCorner];
    _nameTxt.text = delegate.sharedUserModel.name;
    _bdayTxt.text = delegate.sharedUserModel.birthday;
    _emailTxt.text = delegate.sharedUserModel.email;
    
    _imgView.image = [UIImage imageNamed:@"placeholder"];
    _imgView.imageURL = [NSURL URLWithString:delegate.sharedUserModel.profilePicture];
    NSURL *url = [NSURL URLWithString:delegate.sharedUserModel.profilePicture];
    [[AsyncImageLoader sharedLoader] loadImageWithURL:url];
    
//[_imgView roundCorners];
    
}

#pragma mark - Text Field Delegate Methods
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    [self animateTextField:nil up:YES];
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField:nil up:NO];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const int movementDistance = 145; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)dobValueChanged:(id)sender {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    _bdayTxt.text = [NSString stringWithFormat:@"%@", [dateFormatter stringFromDate: _dobPicker.date]];
}

- (IBAction)birthdayPressed:(id)sender {
    _DOBVIEW.hidden = false;
}

- (BOOL) validateFields {
    if(_nameTxt.text.length < 2 || ![Utils validateEmail:_emailTxt.text] || _bdayTxt.text.length < 2) {
        return false;
    }
    return true;
}

- (IBAction)submitPressed:(id)sender {
    
    if([self validateFields]) {
        [CustomLoading showAlertMessage];
        [self makeServerCall];
    }
    else {
        [self presentViewController:[Utils showCustomAlert:@"شئ مفقود" andMessage:@"الرجاء التأكد من تعبئة كافة الحقول مع بيانات صالحة"] animated:YES completion:nil];
    }
}

-(void)makeServerCall{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    NSString *userID = (NSString*)[[NSUserDefaults standardUserDefaults] objectForKey:@"userID"];
    [parameters setValue:userID forKey:@"user_id"];
    [parameters setValue:_nameTxt.text forKey:@"name"];
    [parameters setValue:_bdayTxt.text forKey:@"birthdate"];
    NSData *profileData = UIImagePNGRepresentation(self.imgView.image);

    NSString *urlForEditProfile = [NSString stringWithFormat:@"%@/User/edit_profile",BASE_URL];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];

    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    //    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html",@"text/plain", nil];
    [manager POST:urlForEditProfile parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        [formData appendPartWithFileData:profileData name:@"profile_picture" fileName:@"image.png" mimeType:@"image/png"];
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
        });
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            NSLog(@" %@", responseObject);
        [CustomLoading DismissAlertMessage];
        int flag = [[responseObject objectForKey:@"status"] intValue];
        if(flag == 1) {
            
            User *user = [[User alloc] initWithDictionary:[responseObject objectForKey:@"user_info"]];
            delegate.sharedUserModel.birthday = user.birthday;
            delegate.sharedUserModel.date_time = user.date_time;
            delegate.sharedUserModel.email = user.email;
            delegate.sharedUserModel.gender = user.gender;
            delegate.sharedUserModel.name = user.name;
            delegate.sharedUserModel.profilePicture = user.profilePicture;
            delegate.sharedUserModel.stripeCustomerId = user.stripeCustomerId;
            delegate.sharedUserModel.userID = user.userID;
            
        }

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [CustomLoading DismissAlertMessage];
    }];

}

- (IBAction)backPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

- (IBAction)imgchangeRequestPressed:(id)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle: nil
                                                             delegate: self
                                                    cancelButtonTitle: @"Cancel"
                                               destructiveButtonTitle: nil
                                                    otherButtonTitles: @"Take a new photo",
                                  @"Choose from existing", nil];
    [actionSheet showInView:self.view];
}

- (IBAction)dobDonePressed:(id)sender {
    _DOBVIEW.hidden = true;
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    int i = buttonIndex;
    
    switch(i) {
            
        case 0:
        {
            //Code for camera
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            picker.allowsEditing = YES;
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            [self presentViewController:picker animated:YES completion:NULL];
        }
            break;
        case 1:
        {
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            picker.allowsEditing = YES;
            picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            [self presentViewController:picker animated:YES completion:NULL];
        }
            
        default:
            // Do Nothing.........
            break;
            
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    self.imgView.image = chosenImage;
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [self dismissModalViewControllerAnimated:YES];
}

- (IBAction)backPresed:(id)sender {
}

@end

//
//  InternalTableView.h
//  Joumana
//
//  Created by Ahmed Sadiq on 22/12/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InternalTableView : UIViewController <UITableViewDataSource,UITableViewDelegate>

@property int option;
@property (strong, nonatomic) NSMutableArray *postArray;
@property (strong, nonatomic) IBOutlet UITableView *postTblView;
@property (strong, nonatomic) IBOutlet UILabel *titleLbl;

@property (weak, nonatomic) IBOutlet UILabel *noDataLbl;

@end

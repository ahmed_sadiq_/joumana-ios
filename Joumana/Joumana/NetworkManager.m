//
//  NetworkManager.m
//  LawNote
//
//  Created by Samreen Noor on 22/07/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "NetworkManager.h"

#import "SVProgressHUD.h"

#import "DataManager.h"

@implementation NetworkManager


+ (NSError *)createErrorMessageForObject:(id)responseObject
{
    
    NSString * responseStr = @"";
    
    if (responseObject[@"message"])
        responseStr = [NSString stringWithFormat:@"%@",responseObject[@"message"]];
    else
        responseStr = [NSString stringWithFormat:@"API Response Error!\n%@", responseObject];
    
    
    NSError *error = [NSError errorWithDomain:@"Failed!"
                                         code:100
                                     userInfo:@{
                                                // NSLocalizedDescriptionKey:responseObject[@"message"]
                                                
                                                NSLocalizedDescriptionKey:responseStr
                                                
                                                }];
    NSLog(@"Failed with Response: %@", responseObject);
    return error;
}


+(void) postURI:(NSString *) uri
     parameters:(NSDictionary *) params
        success:(loadSuccess) success
        failure:(loadFailure) failure{
    
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeGradient];
    
    NSURL *baseURL = [[NSURL alloc] initWithString:BASE_URL];
//    
//    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
//    
//    // [manager.requestSerializer setValue:[DataManager sharedManager].authToken forHTTPHeaderField:@"token"];
//    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
//    
//    [manager POST:uri parameters:params success:^(NSURLSessionDataTask *task, id responseObject)
//     {
//         [SVProgressHUD dismiss];
//         
//         BOOL _successCall = [[responseObject valueForKey:@"status"] boolValue] ;
//         
//         if (_successCall){
//             success(responseObject);
//         }
//         else
//         {
//             NSError *error = [self createErrorMessageForObject:responseObject];
//             failure(error);
//         }
//         
//     }
//          failure:^(NSURLSessionDataTask *task, NSError *error)
//     {
//         [SVProgressHUD dismiss];         
//         failure(error);
//     }];
}

+(void)  getURI:(NSString *) uri
     parameters:(NSDictionary *) params
        success:(loadSuccess) success
        failure:(loadFailure) failure{
    
    uri = [uri stringByAddingPercentEscapesUsingEncoding:
           NSUTF8StringEncoding];
    
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeGradient];
    
    NSURL *baseURL = [[NSURL alloc] initWithString:BASE_URL];
    
//    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
//    [manager.requestSerializer setValue:[DataManager sharedManager].authToken forHTTPHeaderField:@"token"];
//    
//    [manager GET:uri parameters:params success:^(NSURLSessionDataTask *task, id responseObject)
//     {
//         [SVProgressHUD dismiss];
//         
//         NSLog(@"responseObject: %@", responseObject);
//         
//         BOOL _successCall = [[responseObject valueForKey:@"status"] boolValue] ;
//         
//         if (_successCall){
//             success(responseObject);
//         }
//         else
//         {
//             NSError *error = [self createErrorMessageForObject:responseObject];
//             failure(error);
//         }
//         
//     }
//         failure:^(NSURLSessionDataTask *task, NSError *error)
//     {
//         [SVProgressHUD dismiss];
//         
//         NSLog(@"Failure: %@", error.localizedDescription);
//         failure(error);
//     }];
}

+(void)  putURI:(NSString *) uri
     parameters:(NSDictionary *) params
        success:(loadSuccess) success
        failure:(loadFailure) failure{
    
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeGradient];
    
    NSURL *baseURL = [[NSURL alloc] initWithString:BASE_URL];
//    
//    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
//    [manager.requestSerializer setValue:[DataManager sharedManager].authToken forHTTPHeaderField:@"token"];
//    
//    [manager PUT:uri parameters:params success:^(NSURLSessionDataTask *task, id responseObject)
//     {
//         [SVProgressHUD dismiss];
//         
//         NSLog(@"responseObject: %@", responseObject);
//         
//         BOOL _successCall = [[responseObject valueForKey:@"success"] boolValue] ;
//         
//         if (_successCall){
//             success(responseObject);
//         }
//         else
//         {
//             NSError *error = [self createErrorMessageForObject:responseObject];
//             failure(error);
//         }
//         
//     }
//         failure:^(NSURLSessionDataTask *task, NSError *error)
//     {
//         [SVProgressHUD dismiss];
//         
//         NSLog(@"Failure: %@", error.localizedDescription);
//         failure(error);
//     }];
}




+(void) postURI:(NSString *) uri
   withFilePath:(NSURL *) path
 attachmentName:(NSString *) paramName
     parameters:(NSDictionary *) params
        success:(loadSuccess) success
        failure:(loadFailure) failure
{
//    NSURL *baseURL = [[NSURL alloc] initWithString:BASE_URL];
//    
//    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc]initWithBaseURL:baseURL];
//    
//    [manager POST:uri parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
//     {
//         
//         [formData appendPartWithFileURL:path name:paramName error:nil];
//         //         [formData appendPartWithFormData:[NSData dataWithContentsOfURL:path] name:paramName];
//         
//     }
//          success:^(AFHTTPRequestOperation *operation, id responseObject) {
//              
//              
//              
//              [SVProgressHUD dismiss];
//              
//              NSLog(@"responseObject: %@", responseObject);
//              
//              BOOL _successCall = [[responseObject valueForKey:@"success"] boolValue] ;
//              
//              if (_successCall){
//                  success(responseObject);
//              }
//              else
//              {
//                  NSError *error = [self createErrorMessageForObject:responseObject];
//                  failure(error);
//              }
//              
//              
//          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//              
//              [SVProgressHUD dismiss];
//              
//              NSLog(@"Failure: %@", error.localizedDescription);
//              failure(error);
//              
//          }];
//    
}





+(void)deleteURI:(NSString *)uri
      parameters:(NSDictionary *)params
         success:(loadSuccess)success
         failure:(loadFailure)failure{
    
//    NSURL *baseURL = [[NSURL alloc] initWithString:BASE_URL];
//    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc]initWithBaseURL:baseURL];
//    
//    
//    [manager DELETE:uri parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        
//        [SVProgressHUD dismiss];
//        
//        NSLog(@"responseObject: %@", responseObject);
//        
//        BOOL _successCall = [[responseObject valueForKey:@"success"] boolValue] ;
//        
//        if (_successCall){
//            success(responseObject);
//        }
//        else
//        {
//            NSError *error = [self createErrorMessageForObject:responseObject];
//            failure(error);
//        }
//        
//        
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        
//        [SVProgressHUD dismiss];
//        
//        NSLog(@"Failure: %@", error.localizedDescription);
//        failure(error);
//        
//    }];
    
    
}

@end

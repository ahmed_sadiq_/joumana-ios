//
//  Transcation.h
//  Joumana
//
//  Created by Ahmed Sadiq on 25/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseEntity.h"

@interface Transcation : BaseEntity

@property (nonatomic, strong) NSString * amount;
@property (nonatomic, strong) NSString * date_time;
@property (nonatomic, strong) NSString * minutes;
@property (nonatomic, strong) NSString * payment_id;
@property (nonatomic, strong) NSString * stripe_customer_id;

- (id)initWithDictionary:(NSDictionary *) responseData;

@end

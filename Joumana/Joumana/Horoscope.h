//
//  Horoscope.h
//  Joumana
//
//  Created by Ahmed Sadiq on 18/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseEntity.h"
@interface Horoscope : BaseEntity

@property (nonatomic, strong) NSString * content;
@property (nonatomic, strong) NSString * date;
@property (nonatomic, strong) NSString * date_time;
@property (nonatomic, strong) NSString * horoscope_id;
@property (nonatomic, strong) NSString * star_id;
@property (nonatomic, strong) NSString * val;

- (id)initWithDictionary:(NSDictionary *) responseData;

@end

//
//  User.m
//  LawNote
//
//  Created by Samreen Noor on 22/07/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "User.h"
#import "Events.h"
#import "BookedCall.h"
#import "ZodiacStar.h"
#import "News.h"
#import "Pictures.h"
#import "WorkingHours.h"
#import "Videos.h"
#import "Transcation.h"

@implementation User
- (id)initWithDictionary:(NSDictionary *) responseData
{
    
    self = [super init];
    
    if (self) {
        
        self.birthday = [self validStringForObject:responseData [@"birthdate"]];
        self.date_time = [self validStringForObject:responseData [@"date_time"]];
        self.gender = [self validStringForObject:responseData [@"gender"]];
        self.stripeCustomerId = [self validStringForObject:responseData [@"stripe_customer_id"]];
        self.userID  = [self validStringForObject:responseData [@"user_id"]];
        self.name  = [self validStringForObject:responseData [@"name"]];
        self.email  = [self validStringForObject:responseData [@"email"]];
        self.profilePicture = [self completeImageURL:responseData [@"profile_picture"]];
        
    }
    
    return self;
}

-(void) populateEvents : (NSArray*) eArray {
    
    self.eventsArray = [[NSMutableArray alloc] init];
    
    for(int i=0; i<eArray.count; i++) {
        NSDictionary *dictTemp = (NSDictionary*) [eArray objectAtIndex:i];
        Events *eventObj = [[Events alloc] initWithDictionary:dictTemp];
        [self.eventsArray addObject:eventObj];
    }
}

-(void) populateBookedCallsTime : (NSArray*) bArray {
    
    self.bookedCallsArray = [[NSMutableArray alloc] init];
    
    for(int i=0; i<bArray.count; i++) {
        NSDictionary *dictTemp = (NSDictionary*) [bArray objectAtIndex:i];
        BookedCall *eventObj = [[BookedCall alloc] initWithDictionary:dictTemp];
        [self.bookedCallsArray addObject:eventObj];
    }
}

-(void) populateZodiacStarsArray : (NSArray*) zArray {
    
    self.zodiacStarsArray = [[NSMutableArray alloc] init];
    
    for(int i=0; i<zArray.count; i++) {
        NSDictionary *dictTemp = (NSDictionary*) [zArray objectAtIndex:i];
        ZodiacStar *eventObj = [[ZodiacStar alloc] initWithDictionary:dictTemp];
        [self.zodiacStarsArray addObject:eventObj];
    }
}

-(void) populateNewsArray : (NSArray*) nArray {
    
    self.newsArray = [[NSMutableArray alloc] init];
    
    for(int i=0; i<nArray.count; i++) {
        NSDictionary *dictTemp = (NSDictionary*) [nArray objectAtIndex:i];
        News *newsObj = [[News alloc] initWithDictionary:dictTemp];
        [self.newsArray addObject:newsObj];
    }
}

-(void) populatePicturesArray : (NSArray*) nArray {
    
    self.picturesArray = [[NSMutableArray alloc] init];
    
    for(int i=0; i<nArray.count; i++) {
        NSDictionary *dictTemp = (NSDictionary*) [nArray objectAtIndex:i];
        Pictures *newsObj = [[Pictures alloc] initWithDictionary:dictTemp];
        [self.picturesArray addObject:newsObj];
    }
}

-(void) populateWorkingHoursArray : (NSArray*) nArray {
    
    self.workingHoursArray = [[NSMutableArray alloc] init];
    
    for(int i=0; i<nArray.count; i++) {
        NSDictionary *dictTemp = (NSDictionary*) [nArray objectAtIndex:i];
        WorkingHours *newsObj = [[WorkingHours alloc] initWithDictionary:dictTemp];
        [self.workingHoursArray addObject:newsObj];
    }
}

-(void) populateVideoArray : (NSArray*) nArray {
    
    self.videosArray = [[NSMutableArray alloc] init];
    
    for(int i=0; i<nArray.count; i++) {
        NSDictionary *dictTemp = (NSDictionary*) [nArray objectAtIndex:i];
        Videos *newsObj = [[Videos alloc] initWithDictionary:dictTemp];
        [self.videosArray addObject:newsObj];
    }
}

-(void) populateTransactionArray : (NSArray*) nArray {
    
    self.transactionArray = [[NSMutableArray alloc] init];
    
    for(int i=0; i<nArray.count; i++) {
        NSDictionary *dictTemp = (NSDictionary*) [nArray objectAtIndex:i];
        Transcation *newsObj = [[Transcation alloc] initWithDictionary:dictTemp];
        [self.transactionArray addObject:newsObj];
    }
    
}


@end

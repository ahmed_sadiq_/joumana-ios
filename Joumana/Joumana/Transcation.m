//
//  Transcation.m
//  Joumana
//
//  Created by Ahmed Sadiq on 25/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "Transcation.h"

@implementation Transcation
- (id)initWithDictionary:(NSDictionary *) responseData
{
    
    self = [super init];
    
    if (self) {
        
        self.amount = [self validStringForObject:responseData [@"amount"]];
        self.date_time = [self validStringForObject:responseData [@"date_time"]];
        self.minutes = [self validStringForObject:responseData [@"minutes"]];
        self.payment_id = [self validStringForObject:responseData [@"payment_id"]];
        self.stripe_customer_id  = [self validStringForObject:responseData [@"stripe_customer_id"]];
        
    }
    
    return self;
}
@end

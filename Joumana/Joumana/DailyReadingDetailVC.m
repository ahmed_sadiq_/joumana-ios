//
//  DailyReadingDetailVC.m
//  Joumana
//
//  Created by Ahmed Sadiq on 23/09/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "DailyReadingDetailVC.h"
#import "Horoscope.h"
#import "CustomLoading.h"
#import "Utils.h"
#import <Social/Social.h>
@interface DailyReadingDetailVC ()

@end

@implementation DailyReadingDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _starTitle.text = _currentStar.starName;
    
    if(_currentStar.horoscopeArray.count > 0) {
        Horoscope *horoscope = [_currentStar.horoscopeArray objectAtIndex:0];
        _detailTxt.text = horoscope.content;
    }
    
    NSString *starName = @"";
    
    switch ([_currentStar.starId intValue]) {
        
        case 1:
            starName = @"aries";
            break;
        case 2:
            starName = @"taurus";
            break;
        case 3:
            starName = @"gemini";
            break;
        case 4:
            starName = @"cancer";
            break;
        case 5:
            starName = @"leo";
            break;
        case 6:
            starName = @"virgo";
            break;
        case 7:
            starName = @"libra";
            break;
        case 8:
            starName = @"scorpio";
            break;
        case 9:
            starName = @"sagittarius";
            break;
        case 10:
            starName = @"capricorn";
            break;
        case 11:
            starName = @"aquarius";
            break;
        case 12:
            starName = @"pisces";
            break;
            
        default:
            break;
    }
    
    NSString *iconName = [NSString stringWithFormat:@"icon%@_white",starName];
   // iconName=[iconName lowercaseString];
    _starImg.image = [UIImage imageNamed:iconName];
    
    int value =  [_currentStar.isLikedBefore intValue];
    if(value)
        [self.heart setBackgroundImage:[UIImage imageNamed:@"heart_filled"] forState:UIControlStateNormal];
    else
        [self.heart setBackgroundImage:[UIImage imageNamed:@"heart_normal"] forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)refreshBtnPressed:(id)sender {
    //[self.navigationController popViewControllerAnimated:true];
}

- (IBAction)menuPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)previousPressed:(id)sender {
    
    [_todayBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_previousBtn setTitleColor:[UIColor colorWithRed:0.816 green:(0.658) blue:(0.384) alpha:1.0] forState:UIControlStateNormal];
    if(_currentStar.horoscopeArray.count > 0) {
        Horoscope *horoscope = [_currentStar.horoscopeArray objectAtIndex:0];
        _detailTxt.text = horoscope.content;
    }
    else {
        _detailTxt.text = @"";
    }
    
}

- (IBAction)todayPressed:(id)sender {
    [_previousBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_todayBtn setTitleColor:[UIColor colorWithRed:0.816 green:(0.658) blue:(0.384) alpha:1.0] forState:UIControlStateNormal];
    if(_currentStar.horoscopeArray.count > 1) {
        Horoscope *horoscope = [_currentStar.horoscopeArray objectAtIndex:1];
        _detailTxt.text = horoscope.content;
    }
    else {
        _detailTxt.text = @"";
    }
    
    
}

- (IBAction)likeBtnPressed:(id)sender {
    
    Horoscope *horoscope = [_currentStar.horoscopeArray objectAtIndex:1];
    int horoID = [horoscope.horoscope_id intValue];
    NSString *horoIDStr = [NSString stringWithFormat:@"%d",horoID];
    [CustomLoading showAlertMessage];
    
    NSString *urlStr = [NSString stringWithFormat:@"%@%@",BASE_URL,@"Horoscope/like_horoscope/"];
    NSURL *url = [NSURL URLWithString:urlStr];
    
    NSMutableDictionary *postParams = [[NSMutableDictionary alloc] init];
    [postParams setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"userID"] forKey:@"user_id"];
    [postParams setObject:horoIDStr forKey:@"horoscope_id"];
    
    NSData *postData = [Utils encodeDictionary:postParams];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        
        [CustomLoading DismissAlertMessage];
        
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int flag = [[result objectForKey:@"status"] intValue];
            
            if(flag == 1) {
                int value = [[result objectForKey:@"value"] intValue];
                if(value){
                    [self.heart setBackgroundImage:[UIImage imageNamed:@"heart_filled"] forState:UIControlStateNormal];
                    _currentStar.isLikedBefore = @"1";
                }
                else{
                    [self.heart setBackgroundImage:[UIImage imageNamed:@"heart_normal"] forState:UIControlStateNormal];
                    _currentStar.isLikedBefore = @"0";
                }
            }
            else {
                
                [self presentViewController:[Utils showCustomAlert:@"حدث خطأ ما" andMessage:[result objectForKey:@"message"]] animated:YES completion:nil];
            }
        }
        else{
            
            [self presentViewController:[Utils showCustomAlert:@"Conection Error" andMessage:@"Please retry after some time"] animated:YES completion:nil];
        }
    }];
    
}

- (IBAction)instaBtnPressed:(id)sender {
    
    UIImage *imageToShare = [self capture];
    
    NSURL *instagramURL = [NSURL URLWithString:@"instagram://app"];
    if ([[UIApplication sharedApplication] canOpenURL:instagramURL])
    {
        UIImage *imageToUse = imageToShare;
        NSString *documentDirectory=[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
        NSString *saveImagePath=[documentDirectory stringByAppendingPathComponent:@"Image.igo"];
        NSData *imageData=UIImagePNGRepresentation(imageToUse);
        [imageData writeToFile:saveImagePath atomically:YES];
        NSURL *imageURL=[NSURL fileURLWithPath:saveImagePath];
        self.documentController=[[UIDocumentInteractionController alloc]init];
        self.documentController = [UIDocumentInteractionController interactionControllerWithURL:imageURL];
        self.documentController.delegate = self;
        self.documentController.annotation = [NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"Testing"], @"InstagramCaption", nil];
        self.documentController.UTI = @"com.instagram.exclusivegram";
        UIViewController *vc = [UIApplication sharedApplication].keyWindow.rootViewController;
        [self.documentController presentOpenInMenuFromRect:CGRectMake(1, 1, 1, 1) inView:vc.view animated:YES];
    }
    else
    {
        NSLog (@"Instagram not found");
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:@"Account Configuration Error"
                                              message:@"Please install instagram application from appstore"
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                       NSLog(@"OK action");
                                   }];
        
        [alertController addAction:okAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
        
    }
}

- (IBAction)facebookBtnPressed:(id)sender {
    
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        
        
        [controller setInitialText:@"برجي اليومي من جومانا الكبيسي"];
        [controller addImage:[self capture]];
        
        [self presentViewController:controller animated:YES completion:Nil];
        
        SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
            if (result == SLComposeViewControllerResultCancelled) {
                
                NSLog(@"delete");
                
            } else
                
            {
                NSLog(@"post");
            }
        };
        controller.completionHandler =myBlock;
        
        
    }else {
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:@"Account Configuration Error"
                                              message:@"Please configure Facebook Account in Settings. "
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                       NSLog(@"OK action");
                                   }];
        
        [alertController addAction:okAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }
    
}

- (IBAction)fbBtnPressed:(id)sender {
    
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        SLComposeViewController *tweetSheet = [SLComposeViewController
                                               composeViewControllerForServiceType:SLServiceTypeTwitter];
        [tweetSheet setInitialText:@"برجي اليومي من جومانا الكبيسي"];
        [tweetSheet addImage:[self capture]];
        [self presentViewController:tweetSheet animated:YES completion:nil];
        
        SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
            if (result == SLComposeViewControllerResultCancelled) {
                NSLog(@"delete");
            } else
            {
                NSLog(@"post");
            }
            //   [composeController dismissViewControllerAnimated:YES completion:Nil];
        };
        tweetSheet.completionHandler =myBlock;
    }
    else {
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:@"Account Configuration Error"
                                              message:@"Please configure Twitter Account in Settings. "
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                       NSLog(@"OK action");
                                   }];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    
}

-(UIImage *)capture{
    
    UIGraphicsBeginImageContext(self.view.bounds.size);
    [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *imageView = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    UIImageWriteToSavedPhotosAlbum(imageView, nil, nil, nil); //if you need to save
    return imageView;
    
}
@end

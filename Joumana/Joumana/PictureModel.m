//
//  PictureModel.m
//  Joumana
//
//  Created by Ahmed Sadiq on 22/12/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "PictureModel.h"

@implementation PictureModel

- (id)initWithDictionary:(NSDictionary *) responseData
{
    
    self = [super init];
    
    if (self) {
        
        self.picture  = [self completeImageURL:responseData [@"picture"]];
    }
    
    return self;
}

@end

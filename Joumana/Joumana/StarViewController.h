//
//  StarViewController.h
//  Joumana
//
//  Created by Ahmed Sadiq on 22/12/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZodiacStar.h"
#import "FSCalendar.h"

@interface StarViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UIDocumentInteractionControllerDelegate>
@property (weak, nonatomic) IBOutlet UIView *calendarView;
@property (weak, nonatomic) IBOutlet UIView *boxView;
@property (strong, nonatomic) IBOutlet UILabel *titleLbl;
@property (strong, nonatomic) IBOutlet UILabel *starName;
@property (strong, nonatomic) IBOutlet UILabel *descLbl;
@property (strong, nonatomic) NSMutableArray *datesArray;
@property (strong, nonatomic) NSMutableArray *postArray;
@property (strong, nonatomic) IBOutlet UITableView *dateTblView;
@property (strong, nonatomic) IBOutlet UIImageView *starImg;
@property int option;
@property (strong, nonatomic) ZodiacStar *currentStar;
@property (strong, nonatomic) IBOutlet UILabel *noDataLbl;
@property (nonatomic, strong) UIDocumentInteractionController *documentController;


@property (weak, nonatomic) FSCalendar *calendar;
@property (weak, nonatomic) UIButton *previousButton;
@property (weak, nonatomic) UIButton *nextButton;
@property (strong, nonatomic) NSDateFormatter *dateFormatter;

@property (strong, nonatomic) NSCalendar *gregorian;

- (void)previousClicked:(id)sender;
- (void)nextClicked:(id)sender;

- (IBAction)instaPressed:(id)sender;
- (IBAction)fbPressed:(id)sender;
- (IBAction)twitterPressed:(id)sender;


- (IBAction)backPressed:(id)sender;

@end

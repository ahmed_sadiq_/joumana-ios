//
//  NewsCell.h
//  Joumana
//
//  Created by Ahmed Sadiq on 21/09/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"
@interface NewsCell : UITableViewCell
@property (strong, nonatomic) IBOutlet AsyncImageView *imgView;
@property (strong, nonatomic) IBOutlet UILabel *eventTxt;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@end

//
//  GalleryVC.m
//  Joumana
//
//  Created by Ahmed Sadiq on 21/09/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "GalleryVC.h"
#import "Pictures.h"
#import "Videos.h"
#import "StarsCollectionView.h"
@interface GalleryVC ()

@end

@implementation GalleryVC
@synthesize moviePlayer;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    
    
    
}

- (IBAction)option1Pressed:(id)sender {
    
    [self goToScreen:1];
    
}
- (IBAction)option2Pressed:(id)sender {
    
    [self goToScreen:2];
    
}
- (IBAction)option3Pressed:(id)sender {
    
    [self goToScreen:3];
    
}
- (IBAction)option4Pressed:(id)sender {
    
    [self goToScreen:4];
    
}


- (void) goToScreen : (int) optionNum {
    StarsCollectionView *starViewCont = [[StarsCollectionView alloc] initWithNibName:@"StarsCollectionView" bundle:nil];
    starViewCont.option = optionNum;
    [self.navigationController pushViewController:starViewCont animated:YES];
    [self.navigationController setNavigationBarHidden:YES];
}

- (void) populateUpperScroll {
    
    NSString *counterStr = [NSString stringWithFormat:@"الصور %d",(int)delegate.sharedUserModel.picturesArray.count];
    
    imagesCount.text = counterStr;
    
    int x = 0;
    int y = 0;
    
    for(int i=0; i<delegate.sharedUserModel.picturesArray.count; i++) {
        
        Pictures *pictureObj = (Pictures*)[delegate.sharedUserModel.picturesArray objectAtIndex:i];
        
        AsyncImageView *imgView = [[AsyncImageView alloc] initWithFrame:CGRectMake(x, y, 135, 135)];
        imgView.image = [UIImage imageNamed:@"placeholder"];
        imgView.imageURL = [NSURL URLWithString:pictureObj.picture];
        NSURL *url = [NSURL URLWithString:pictureObj.picture];
        [[AsyncImageLoader sharedLoader] loadImageWithURL:url];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button addTarget:self
                   action:@selector(imagePressed:)
         forControlEvents:UIControlEventTouchUpInside];
        button.frame = CGRectMake(x, y, 135.0, 135.0);
        button.tag = i;

        [_upperScroll addSubview:imgView];
        [_upperScroll addSubview:button];
        x = x+138;
    }
}

- (void) populateMiddleScroll {
    
    NSString *counterStr = [NSString stringWithFormat:@"الفيديو %d",(int)delegate.sharedUserModel.videosArray.count];
    
    videoCount.text = counterStr;
    
    int x = 0;
    int y = 0;
    
    for(int i=0; i<delegate.sharedUserModel.videosArray.count; i++) {
        
        Videos *videoObj = (Videos*)[delegate.sharedUserModel.videosArray objectAtIndex:i];
        
        AsyncImageView *imgView = [[AsyncImageView alloc] initWithFrame:CGRectMake(x, y, 135, 135)];
        
        imgView.image = [UIImage imageNamed:@"placeholder"];
        
        
        imgView.imageURL = [NSURL URLWithString:videoObj.thumbnail];
        NSURL *url = [NSURL URLWithString:videoObj.thumbnail];
        [[AsyncImageLoader sharedLoader] loadImageWithURL:url];
        
        UIImageView *playImg = [[UIImageView alloc] initWithFrame:CGRectMake(x, y, 135, 135)];
        playImg.image = [UIImage imageNamed:@"video-play"];
        playImg.contentMode = UIViewContentModeScaleAspectFit;
        playImg.clipsToBounds = YES;
        
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button addTarget:self
                   action:@selector(videoPressed:)
         forControlEvents:UIControlEventTouchUpInside];
        button.frame = CGRectMake(x, y, 135.0, 135.0);
        button.tag = i;
        
        [_middleScroll addSubview:imgView];
        [_middleScroll addSubview:playImg];
        [_middleScroll addSubview:button];
        x = x+138;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)refreshPressed:(id)sender {
    //[self.navigationController popViewControllerAnimated:true];
}

- (IBAction)menuPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

- (IBAction)imagePressed:(id)sender {
    
    UIButton *senderBtn = (UIButton*)sender;
    
    Pictures *pictureObj = (Pictures*)[delegate.sharedUserModel.picturesArray objectAtIndex:senderBtn.tag];
    
    _detailImg.imageURL = [NSURL URLWithString:pictureObj.picture];
    NSURL *url = [NSURL URLWithString:pictureObj.picture];
    [[AsyncImageLoader sharedLoader] loadImageWithURL:url];
    
    _detailBg.hidden = false;
    
}

- (IBAction)videoPressed:(id)sender {
    UIButton *senderBtn = (UIButton*)sender;
    
    Videos *videoObj = (Videos*)[delegate.sharedUserModel.videosArray objectAtIndex:senderBtn.tag];
    //_videoView.hidden = false;
    
    
    NSURL *url=[[NSURL alloc] initWithString:videoObj.video];
    moviePlayer=[[MPMoviePlayerController alloc] initWithContentURL:url];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(moviePlayBackDidFinish:) name:MPMoviePlayerPlaybackDidFinishNotification object:moviePlayer];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(moviePlayBackDonePressed:) name:MPMoviePlayerDidExitFullscreenNotification object:moviePlayer];
    
    moviePlayer.controlStyle=MPMovieControlStyleDefault;
    //moviePlayer.shouldAutoplay=NO;
    [moviePlayer play];
    [self.view addSubview:moviePlayer.view];
    [moviePlayer setFullscreen:YES animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)detailDonePressed:(id)sender {
    _detailBg.hidden = true;
}

- (IBAction)openDetailPressed:(id)sender {
    _detailBg.hidden = false;
}
- (IBAction)videoBackPressed:(id)sender {
}


- (void) moviePlayBackDonePressed:(NSNotification*)notification
{
    [moviePlayer stop];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerDidExitFullscreenNotification object:moviePlayer];
    
    
    if ([moviePlayer respondsToSelector:@selector(setFullscreen:animated:)])
    {
        [moviePlayer.view removeFromSuperview];
    }
    moviePlayer=nil;
}

- (void) moviePlayBackDidFinish:(NSNotification*)notification
{
    [moviePlayer stop];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:moviePlayer];
    
    if ([moviePlayer respondsToSelector:@selector(setFullscreen:animated:)])
    {
        [moviePlayer.view removeFromSuperview];
    }
}
@end

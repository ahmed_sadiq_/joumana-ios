//
//  BookCallVC.h
//  Joumana
//
//  Created by Ahmed Sadiq on 22/09/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NIDropDown.h"
#import "HSDatePickerViewController.h"
#import "FSCalendar.h"
#import "AppDelegate.h"

@interface BookCallVC : UIViewController <NIDropDownDelegate,FSCalendarDataSource, FSCalendarDelegate,UIWebViewDelegate> {
    NIDropDown *dropDown;
    NSArray * arr;
    int weekdayInt;
    
    NSDate *startTime;
    NSDate *endTime;
    AppDelegate *delegate;
    
    BOOL minutesSelected;
    BOOL timeSelected;
    NSString *selectedDate;
}
@property (strong, nonatomic) IBOutlet UIButton *durationBtn;
@property (strong, nonatomic) IBOutlet UIButton *availableTimeBtn;
@property (weak, nonatomic) FSCalendar *calendar;
@property (weak, nonatomic) UIButton *previousButton;
@property (weak, nonatomic) UIButton *nextButton;
@property (strong, nonatomic) NSDateFormatter *dateFormatter;

@property (strong, nonatomic) NSCalendar *gregorian;

- (void)previousClicked:(id)sender;
- (void)nextClicked:(id)sender;

@property (strong, nonatomic) IBOutlet UIView *timePicker;
@property (strong, nonatomic) IBOutlet UIDatePicker *hoursPicker;
@property (weak, nonatomic) IBOutlet UIPickerView *timePickerP;


@property (strong, nonatomic) IBOutlet UIView *paymentMainView;
@property (strong, nonatomic) IBOutlet UIWebView *paymentWebView;
- (IBAction)paymentMainViewBackPressed:(id)sender;


@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *actInd;
- (IBAction)selectMinutesPressed:(id)sender;
- (IBAction)confirmPressed:(id)sender;
- (IBAction)timePickerDonePressed:(id)sender;

@end

//
//  DailyReadingDetailVC.h
//  Joumana
//
//  Created by Ahmed Sadiq on 23/09/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZodiacStar.h"

@interface DailyReadingDetailVC : UIViewController<UIDocumentInteractionControllerDelegate>

@property (strong, nonatomic) IBOutlet UIImageView *starImg;
@property (strong, nonatomic) IBOutlet UILabel *starTitle;
@property (strong, nonatomic) IBOutlet UIButton *todayBtn;
@property (strong, nonatomic) IBOutlet UIButton *previousBtn;
@property (strong, nonatomic) IBOutlet UITextView *detailTxt;
@property (nonatomic, strong) UIDocumentInteractionController *documentController;
@property (strong, nonatomic) ZodiacStar *currentStar;

- (IBAction)previousPressed:(id)sender;
- (IBAction)todayPressed:(id)sender;

- (IBAction)likeBtnPressed:(id)sender;
- (IBAction)instaBtnPressed:(id)sender;
- (IBAction)facebookBtnPressed:(id)sender;
- (IBAction)fbBtnPressed:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *heart;
@property (weak, nonatomic) IBOutlet UIButton *twitter;
@property (weak, nonatomic) IBOutlet UIButton *insta;
@property (weak, nonatomic) IBOutlet UIButton *facebook;
@end

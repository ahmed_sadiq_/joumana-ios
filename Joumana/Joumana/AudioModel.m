//
//  AudioModel.m
//  Joumana
//
//  Created by Ahmed Sadiq on 22/12/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "AudioModel.h"

@implementation AudioModel

- (id)initWithDictionary:(NSDictionary *) responseData
{
    
    self = [super init];
    
    if (self) {
        
        self.audio = [self completeImageURL:responseData [@"audio_link"]];
        
    }
    
    return self;
}

@end

//
//  TransactionHistoryVC.m
//  Joumana
//
//  Created by Ahmed Sadiq on 25/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "TransactionHistoryVC.h"
#import "TransactionHistoryCell.h"
#import "Transcation.h"

@interface TransactionHistoryVC ()

@end

@implementation TransactionHistoryVC
@synthesize delegate;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return delegate.sharedUserModel.transactionArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TransactionHistoryCell *cell = (TransactionHistoryCell *)[_transTbl dequeueReusableCellWithIdentifier:nil];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"TransactionHistoryCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    Transcation *tModel = [delegate.sharedUserModel.transactionArray objectAtIndex:indexPath.row];
    NSString *dateStr = tModel.date_time;
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    NSDate *date = [dateFormat dateFromString:dateStr];
    
    int duration = [tModel.minutes intValue];
    
    NSDate *addedDate = [date dateByAddingTimeInterval:duration*60];
    
    NSDateFormatter* df = [[NSDateFormatter alloc]init];
    [df setDateFormat:@"MMMM dd, yyyy"];
    NSString *monthStr = [df stringFromDate:date];
    
    NSDateFormatter* df1 = [[NSDateFormatter alloc]init];
    [df1 setDateFormat:@"h:mm"];
    NSString *timeStr = [df1 stringFromDate:date];
    
    NSDateFormatter* df2 = [[NSDateFormatter alloc]init];
    [df2 setDateFormat:@"h:mm"];
    NSString *timeEndStr = [df2 stringFromDate:addedDate];
    
    NSString *str = [NSString stringWithFormat:@"%@ TO %@",timeStr,timeEndStr];
    
    cell.dateLbl.text = monthStr;
    cell.timeLbl.text = str;
    cell.amountLbl.text = [NSString stringWithFormat:@"$ %@",tModel.amount];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
    
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 75;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}


@end

//
//  SettingsUpperLabelCell.m
//  Joumana
//
//  Created by Ahmed Sadiq on 27/02/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "SettingsUpperLabelCell.h"

@implementation SettingsUpperLabelCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

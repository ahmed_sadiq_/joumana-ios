//
//  Videos.h
//  Joumana
//
//  Created by Ahmed Sadiq on 18/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseEntity.h"

@interface Videos : BaseEntity

@property (nonatomic, strong) NSString * date_time;
@property (nonatomic, strong) NSString * video;
@property (nonatomic, strong) NSString * video_id;
@property (nonatomic, strong) NSString * thumbnail;

- (id)initWithDictionary:(NSDictionary *) responseData;

@end

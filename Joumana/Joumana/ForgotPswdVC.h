//
//  ForgotPswdVC.h
//  Joumana
//
//  Created by Ahmed Sadiq on 26/09/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgotPswdVC : UIViewController
@property (strong, nonatomic) IBOutlet UITextField *email;

- (IBAction)backToPasswordPressed:(id)sender;
- (IBAction)resetPswdPressed:(id)sender;
@end

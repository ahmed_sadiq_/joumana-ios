//
//  User.h
//  LawNote
//
//  Created by Samreen Noor on 22/07/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "BaseEntity.h"
#import "CreditCard.h"

@interface User : BaseEntity
@property (nonatomic, strong) NSString * name;
@property (nonatomic, strong) NSString * email;
@property (nonatomic, strong) NSString * userID;
@property (nonatomic, strong) NSString * firstName;
@property (nonatomic, strong) NSString * lastName;
@property (nonatomic, strong) NSString * gender;
@property (strong, nonatomic) NSString * profilePicture;
@property (strong, nonatomic) NSString * birthday;
@property (strong, nonatomic) NSString * date_time;
@property (strong, nonatomic) NSString * stripeCustomerId;
@property (strong, nonatomic) CreditCard * creditCardObj;
@property (strong, nonatomic) NSMutableArray * eventsArray;
@property (strong, nonatomic) NSMutableArray * bookedCallsArray;
@property (strong, nonatomic) NSMutableArray * zodiacStarsArray;
@property (strong, nonatomic) NSMutableArray * newsArray;
@property (strong, nonatomic) NSMutableArray * picturesArray;
@property (strong, nonatomic) NSMutableArray * workingHoursArray;
@property (strong, nonatomic) NSMutableArray * videosArray;
@property (strong, nonatomic) NSMutableArray * transactionArray;



-(id)initWithDictionary:(NSDictionary *) responseData;

-(void) populateEvents : (NSArray*) eventsArray;
-(void) populateBookedCallsTime : (NSArray*) bArray;
-(void) populateZodiacStarsArray : (NSArray*) zArray;
-(void) populateNewsArray : (NSArray*) nArray;
-(void) populatePicturesArray : (NSArray*) nArray;
-(void) populateWorkingHoursArray : (NSArray*) nArray;
-(void) populateVideoArray : (NSArray*) nArray;
-(void) populateTransactionArray : (NSArray*) nArray;


@end

//
//  SettingsVC.m
//  Joumana
//
//  Created by Ahmed Sadiq on 25/09/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "SettingsVC.h"
#import "SettingsLowerCell.h"
#import "SettingsUpperCell.h"
#import "SettingsUpperLabelCell.h"
#import "EditProfileVC.h"
#import "TransactionHistoryVC.h"
#import "BillingInformation.h"
#import "UIImageView+RoundImage.h"
#import "BookedCall.h"
#import "CallerScreenVC.h"
#import "ViewController.h"
#import "AppDelegate.h"
#import "NSString+Date.h"
#import "CustomLoading.h"
#import "Utils.h"

@interface SettingsVC ()

@end

@implementation SettingsVC
@synthesize expandTableView, dataModelArray;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    _titleLbl.text = [NSString stringWithFormat:@"%@", delegate.sharedUserModel.name];
    _imgView.imageURL = [NSURL URLWithString:delegate.sharedUserModel.profilePicture];
    NSURL *url = [NSURL URLWithString:delegate.sharedUserModel.profilePicture];
    [[AsyncImageLoader sharedLoader] loadImageWithURL:url];
    
    [_lowerTblView reloadData];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from
    
    delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    
    _titleLbl.text = [NSString stringWithFormat:@"%@",delegate.sharedUserModel.name];
    
    _imgView.image = [UIImage imageNamed:@"placeholder"];
    
    [self.imgView.layer setBorderColor:[UIColor colorWithRed:0.816 green:(0.658) blue:(0.384) alpha:1.0].CGColor];
    self.imgView.layer.borderWidth = 1.0f;
    self.imgView.layer.masksToBounds = true;
    
    [_imgView roundImageCorner];
    
    NSDateFormatter *dateformate1=[[NSDateFormatter alloc]init];
    [dateformate1 setDateFormat:@"MMM dd, yyyy"]; // Date formater
    NSString *date = [dateformate1 stringFromDate:[NSDate date]];
    
    NSDateFormatter *dateformate = [[NSDateFormatter alloc] init] ;
    [dateformate setTimeZone:[NSTimeZone timeZoneWithName:@"Australia/Sydney"]];
    [dateformate setDateFormat:@"EEEE"];
    
    _dateLbl.text = [NSString stringWithFormat:@"%@, %@",[dateformate stringFromDate:[NSDate date]],date];
    
    self.lowerTblView.dataSource = self;
    self.lowerTblView.delegate = self;
    
    
    [self.lowerTblView reloadData];
    
    if(self.lowerTblView) {
        self.refreshControl = [[UIRefreshControl alloc] init];
        self.refreshControl.backgroundColor = [UIColor clearColor];
        self.refreshControl.tintColor = [UIColor whiteColor];
        [self.refreshControl addTarget:self action:@selector(updateTblView) forControlEvents:UIControlEventValueChanged];
        //self.lowerTblView.refreshControl = self.refreshControl;
        
        [self.lowerTblView addSubview:self.refreshControl];
    }
}

-(void) updateTblView {
    [self getCalls];
}

- (IBAction)menuPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (UIFont *) fontForParents {
    return [UIFont fontWithName:@"American Typewriter" size:18];
}

- (UIFont *) fontForChildren {
    return [UIFont fontWithName:@"American Typewriter" size:18];
}

/*
 - (UIImage *) selectionIndicatorIcon {
 return [UIImage imageNamed:@"green_checkmark"];
 }
 */
#pragma mark - JKExpandTableViewDataSource
- (NSInteger) numberOfParentCells {
    return [self.dataModelArray count];
}

- (NSInteger) numberOfChildCellsUnderParentIndex:(NSInteger) parentIndex {
    NSMutableArray *childArray = [self.dataModelArray objectAtIndex:parentIndex];
    return [childArray count];
}

- (NSString *) labelForParentCellAtIndex:(NSInteger) parentIndex {
    //return [NSString stringWithFormat:@"parent %ld", (long)parentIndex];
    
    switch (parentIndex) {
        case 0:
            return @"معلومات الملف الشخصي";
            break;
        case 1:
            return @"الأشعارات";
            break;
        case 2:
            return @"فيس بوك";
            break;
        case 3:
            return @"خروج";
            break;
            
        default:
            return @"Profile Information";
            break;
    }
}

- (NSString *) imageNameForParentCellAtIndex:(NSInteger) parentIndex {
    //return [NSString stringWithFormat:@"parent %ld", (long)parentIndex];
    
    switch (parentIndex) {
        case 0:
            return @"imagessettings10b_iconprofile";
            break;
        case 1:
        {
            BOOL isNotificationOff =  [[NSUserDefaults standardUserDefaults] boolForKey:@"isNotificationOff"];
            if(isNotificationOff) {
                return @"switch_on";
                
            }
            else {
                return @"switch_off";
            }
            
            break;
        }
            
        case 2:
            return @"facebook";
            break;
        case 3:
            return @"logout";
            break;
            
        default:
            return @"";
            break;
    }
}

- (BOOL) shouldDisplaySelectedStateForCellAtChildIndex:(NSInteger) childIndex withinParentCellIndex:(NSInteger) parentIndex {
    NSMutableArray *childArray = [self.dataModelArray objectAtIndex:parentIndex];
    return [[childArray objectAtIndex:childIndex] boolValue];
}


- (BOOL) shouldRotateIconForParentOnToggle {
    return YES;
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(tableView.tag == 0) {
        return delegate.sharedUserModel.bookedCallsArray.count+1;
    }
    else {
        return 4;
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView.tag == 0) {
        if(indexPath.row == 0) {
            static NSString *simpleTableIdentifier = @"SettingsUpperLabelCell";
            SettingsUpperLabelCell *cell = (SettingsUpperLabelCell *)[_lowerTblView dequeueReusableCellWithIdentifier:nil];
            if (cell == nil)
            {
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"SettingsUpperLabelCell" owner:self options:nil];
                cell = [nib objectAtIndex:0];
            }
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            return cell;
        }
        else {
            static NSString *simpleTableIdentifier = @"SettingsLowerCell";
            SettingsLowerCell *cell = (SettingsLowerCell *)[_lowerTblView dequeueReusableCellWithIdentifier:nil];
            if (cell == nil)
            {
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"SettingsLowerCell" owner:self options:nil];
                cell = [nib objectAtIndex:0];
            }
            
            BookedCall *bCall =  [delegate.sharedUserModel.bookedCallsArray objectAtIndex:indexPath.row - 1];
            //bCall.rem_time = [[NSString alloc]getRemaningTimeWithCallStartTime:bCall.start_time];
            int remtime = [bCall.rem_time intValue];
            
            if(remtime < 61) {
                cell.view2.hidden = true;
                
            }
            else {
                cell.view2.hidden = false;
                cell.remTimeView.hidden = true;
                cell.dateLbl.hidden = true;
                cell.timeLbl.hidden = true;
            }
            
            cell.minRemLbl.text = [NSString stringWithFormat:@"%d",remtime];
            cell.dateLbl.text =[[NSString alloc]convertDateINtoLocalTimezoneWithFormate:@"MMM dd, yyyy" andDate:bCall.end_time];
            cell.timeLbl.text = bCall.call_time;
            
            cell.dateLblView2.text = [[NSString alloc]convertDateINtoLocalTimezoneWithFormate:@"MMM dd, yyyy" andDate:bCall.end_time];;
            cell.timeLblView2.text = bCall.call_time;
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            return cell;
        }
        
    }
    else {
        static NSString *simpleTableIdentifier = @"SettingsUpperCell";
        SettingsUpperCell *cell = (SettingsUpperCell *)[expandTableView dequeueReusableCellWithIdentifier:nil];
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"SettingsUpperCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.titleLbl.text = [self labelForParentCellAtIndex:indexPath.row];
        cell.sideIcon.image = [UIImage imageNamed:[self imageNameForParentCellAtIndex:indexPath.row]];
        return cell;
    }
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView.tag ==1) {
        return 60;
    }
    else {
        if(indexPath.row ==0)
            return 43;
        return 100;
    }
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(tableView.tag ==1) {
        if(indexPath.row == 0) {
            EditProfileVC *homeVC = [[EditProfileVC alloc] initWithNibName:@"EditProfileVC" bundle:nil];
            [self.navigationController pushViewController:homeVC animated:YES];
            [self.navigationController setNavigationBarHidden:YES];
        }
        else if(indexPath.row == 1) {
            
            /*BillingInformation *homeVC = [[BillingInformation alloc] initWithNibName:@"BillingInformation" bundle:nil];
             [self.navigationController pushViewController:homeVC animated:YES];
             [self.navigationController setNavigationBarHidden:YES];*/
            
            BOOL isNotificationOff =  [[NSUserDefaults standardUserDefaults] boolForKey:@"isNotificationOff"];
            if(isNotificationOff) {
                [[NSUserDefaults standardUserDefaults] setBool:false forKey:@"isNotificationOff"];
            }
            else {
                [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"isNotificationOff"];
            }
            [[NSUserDefaults standardUserDefaults] synchronize];
            [expandTableView reloadData];
            
        }
        else if(indexPath.row == 2) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"https://www.facebook.com/joumanakoubaissi/"]];

        }
        else if(indexPath.row == 3) {
            /*TransactionHistoryVC *homeVC = [[TransactionHistoryVC alloc] initWithNibName:@"TransactionHistoryVC" bundle:nil];
             [self.navigationController pushViewController:homeVC animated:YES];
             [self.navigationController setNavigationBarHidden:YES];*/
            
            [[NSUserDefaults standardUserDefaults] setBool:false forKey:@"isLoggedIn"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            ViewController *viewController = [[ViewController alloc] initWithNibName:@"ViewController" bundle:nil];
            self.navController = [[UINavigationController alloc] initWithRootViewController:viewController];
            self.navController.navigationBar.hidden = YES;
            [[[UIApplication sharedApplication]delegate] window].rootViewController = self.navController;
        }
    }
    else {
        if(indexPath.row > 0) {
            BookedCall *bCall =  [delegate.sharedUserModel.bookedCallsArray objectAtIndex:indexPath.row - 1];
            int remtime = [bCall.rem_time intValue];
            if(remtime <= 0) {
                CallerScreenVC *homeVC = [[CallerScreenVC alloc] initWithNibName:@"CallerScreenVC" bundle:nil];
                homeVC.timeDuration = [bCall.duration intValue];
                //homeVC.timeDuration =[[[NSString alloc] getCalldurationWithEndTime:bCall.end_time] intValue];
                homeVC.bCall = bCall;
                [self.navigationController pushViewController:homeVC animated:YES];
                [self.navigationController setNavigationBarHidden:YES];
            }
        }
        
    }
}

- (IBAction)bookingPressed:(id)sender {
    
    _lowerTblView.hidden = false;
    expandTableView.hidden = true;
    
    [_bookingBtn setBackgroundImage:[UIImage imageNamed:@"selected"] forState:UIControlStateNormal];
    [_settingsBtn setBackgroundImage:[UIImage imageNamed:@"unselected"] forState:UIControlStateNormal];
}

- (IBAction)settingsPressed:(id)sender {
    
    _lowerTblView.hidden = true;
    expandTableView.hidden = false;
    
    [_bookingBtn setBackgroundImage:[UIImage imageNamed:@"unselected"] forState:UIControlStateNormal];
    [_settingsBtn setBackgroundImage:[UIImage imageNamed:@"selected"] forState:UIControlStateNormal];
}

-(void) getCalls{
  
    NSString *strSecondPart = [NSString stringWithFormat:@"/Call/get_user_specific_booked_calls2?user_id=%@",delegate.sharedUserModel.userID];
    NSString *urlStr = [NSString stringWithFormat:@"%@%@",BASE_URL,strSecondPart];
    
    urlStr = [urlStr stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    NSURL *url = [NSURL URLWithString:urlStr];
   // [CustomLoading showAlertMessage];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"GET"];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        [self.refreshControl endRefreshing];

        [CustomLoading DismissAlertMessage];
        
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int flag = [[result objectForKey:@"status"] intValue];
            if(flag == 1) {
                NSMutableArray *bookCallsArray = [[NSMutableArray alloc] init];
                NSArray *postArray = [result objectForKey:@"user_specific_booked_calls"];
                for(int i=0; i<postArray.count; i++) {
                    NSDictionary *postDict = [postArray objectAtIndex:i];
                    [bookCallsArray addObject:[[BookedCall alloc] initWithDictionary:postDict]];
                   }
                delegate.sharedUserModel.bookedCallsArray  = bookCallsArray.mutableCopy;
                
                [_lowerTblView reloadData];

            }
            else {
                
                [self presentViewController:[Utils showCustomAlert:@"حدث خطأ ما" andMessage:[result objectForKey:@"message"]] animated:YES completion:nil];
            }
        }
        else{
            
            [self presentViewController:[Utils showCustomAlert:@"Conection Error" andMessage:@"Please retry after some time"] animated:YES completion:nil];
        }
    }];
    
    
}

@end

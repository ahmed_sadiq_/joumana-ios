//
//  SignInVC.m
//  Joumana
//
//  Created by Ahmed Sadiq on 20/09/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "SignInVC.h"
#import "HomeVC.h"
#import "ForgotPswdVC.h"
#import "Utils.h"
#import "User.h"
#import "CreditCard.h"
#import "AppDelegate.h"
#import "CustomLoading.h"
#import "CustomLoading.h"
@interface SignInVC ()

@end

@implementation SignInVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.email setValue:[UIColor whiteColor]
              forKeyPath:@"_placeholderLabel.textColor"];
    [self.pswd setValue:[UIColor whiteColor]
             forKeyPath:@"_placeholderLabel.textColor"];
    
    
}

- (IBAction)backPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}


-(void)dismissKeyboard
{
    [self.view endEditing:true];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
- (IBAction)signInPressed:(id)sender {
    [self.view endEditing:true];
    
    
    if([self validateFields]) {
        
        AppDelegate *del = (AppDelegate*)[UIApplication sharedApplication].delegate;
        
        [CustomLoading showAlertMessage];
        
        NSString *urlStr = [NSString stringWithFormat:@"%@%@",BASE_URL,@"User/signin2"];
        NSURL *url = [NSURL URLWithString:urlStr];
        
        NSMutableDictionary *postParams = [[NSMutableDictionary alloc] init];
        [postParams setObject:_email.text forKey:@"email"];
        [postParams setObject:_pswd.text forKey:@"password"];
        [postParams setObject:@"2" forKey:@"type"]; // for iOS
        [postParams setObject:del.deviceToken forKey:@"device_id"];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSString *date_time = [NSString stringWithFormat:@"%@", [dateFormatter stringFromDate: [NSDate date]]];
        
        [postParams setObject:date_time forKey:@"date_time"];
        
        NSData *postData = [Utils encodeDictionary:postParams];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:url];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:postData];
        
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
            
            [CustomLoading DismissAlertMessage];
            
            if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
            {
                NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                int flag = [[result objectForKey:@"status"] intValue];
                
                if(flag == 1) {
                    
                    User *user = [[User alloc] initWithDictionary:[result objectForKey:@"user_data"]];
                    [user populateEvents:(NSArray*)[result objectForKey:@"events"]];
                    [user populateBookedCallsTime:(NSArray*)[result objectForKey:@"user_specific_booked_calls"]];
                    [user populateZodiacStarsArray:(NSArray*)[result objectForKey:@"hoscopesro"]];
                    [user populateNewsArray:(NSArray*)[result objectForKey:@"news"]];
                    [user populatePicturesArray:(NSArray*)[result objectForKey:@"pictures"]];
                    [user populateWorkingHoursArray:(NSArray*)[result objectForKey:@"working_hours"]];
                    [user populateVideoArray:(NSArray*)[result objectForKey:@"videos"]];
                    [user populateTransactionArray:(NSArray*)[result objectForKey:@"transactions_history"]];
                    CreditCard *ccObj = [[CreditCard alloc] initWithDictionary:[result objectForKey:@"credit_card_info"]];
                    user.creditCardObj = ccObj;
                    
                    AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
                    appDel.sharedUserModel = user;
                    
                    [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"isLoggedIn"];
                    [[NSUserDefaults standardUserDefaults] setObject:user.userID forKey:@"userID"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    HomeVC *homeVC = [[HomeVC alloc] initWithNibName:@"HomeVC" bundle:nil];
                    [self.navigationController pushViewController:homeVC animated:YES];
                    //[self.navigationController setNavigationBarHidden:YES];
                }
                else {
                    
                    [self presentViewController:[Utils showCustomAlert:@"حدث خطأ ما" andMessage:[result objectForKey:@"message"]] animated:YES completion:nil];
                    
                }
            }
            else{
                
                [self presentViewController:[Utils showCustomAlert:@"Conection Error" andMessage:@"Please retry after some time"] animated:YES completion:nil];
            }
        }];
    }
    else {
        [self presentViewController:[Utils showCustomAlert:@"شئ مفقود" andMessage:@"الرجاء التأكد من تعبئة كافة الحقول مع بيانات صالحة"] animated:YES completion:nil];
        
    }
}

#pragma mark - Text Field Delegate Methods
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    [self animateTextField:nil up:YES];
    
    tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField:nil up:NO];
    
    [self.view removeGestureRecognizer:tap];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const int movementDistance = 145; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context:nil];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration: movementDuration];
    if(self.view.frame.origin.y < 0 || up)
        self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
    
}
- (IBAction)forgotPswdPressed:(id)sender {
    [self.view endEditing:true];
    
    ForgotPswdVC *forgotPswdVC = [[ForgotPswdVC alloc] initWithNibName:@"ForgotPswdVC" bundle:nil];
    [self.navigationController pushViewController:forgotPswdVC animated:YES];
    [self.navigationController setNavigationBarHidden:YES];
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    // hide nav bar
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [self animateTextField:nil up:NO];
    // enable slide-back
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
        self.navigationController.interactivePopGestureRecognizer.delegate = self;
    }
}


- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    return YES;
}

- (BOOL) validateFields {
    if(!isLoginFromNumber) {
        if(![Utils validateEmail:_email.text] ||  _pswd.text.length < 1) {
            return false;
        }
        else return true;
    }
    else {
        if(![Utils validatePhoneNumber:_email.text] ||  _pswd.text.length < 1) {
            return false;
        }
        else return true;
    }
    
    return true;
}

- (IBAction)fbPressed:(id)sender {
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logInWithReadPermissions: @[@"public_profile", @"email",@"user_birthday"] fromViewController:self
                            handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                                if (error) {
                                    // show error message
                                    NSLog(@"Process error");
                                } else if (result.isCancelled) {
                                    // show cancelation message
                                    NSLog(@"Cancelled");
                                } else {
                                    NSLog(@"Logged in");
                                    [CustomLoading showAlertMessage];
                                    [self fetchUserInfo];
                                }
                            }];
    
}

-(void)fetchUserInfo
{
    if ([FBSDKAccessToken currentAccessToken])
    {
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"id, name, link, first_name, last_name, picture.type(normal), email, birthday, gender"}]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
             [CustomLoading DismissAlertMessage];
             if (!error)
             {
                 emailStr      = [result objectForKey:@"email"];
                 name = [result objectForKey:@"name"];
                 NSString *userId =     [result objectForKey:@"id"];
                 profilePicUrl  =     [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?width=200&height=200", userId];
                 gender       = [result objectForKey:@"gender"];
                 [self buildSocialServiceCall];
             }
             else
             {
                 NSLog(@"Error %@",error);
             }
         }];
    }
}

- (void) buildSocialServiceCall {
    
    
    [CustomLoading showAlertMessage];
    
    NSString *urlStr = [NSString stringWithFormat:@"%@%@",BASE_URL,@"User/social_login"];
    NSURL *url = [NSURL URLWithString:urlStr];
    
    NSMutableDictionary *postParams = [[NSMutableDictionary alloc] init];
    [postParams setObject:name forKey:@"name"];
    [postParams setObject:emailStr forKey:@"email"];
    [postParams setObject:profilePicUrl forKey:@"profile_picture"];
    [postParams setObject:gender forKey:@"gender"];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *date_time = [NSString stringWithFormat:@"%@", [dateFormatter stringFromDate: [NSDate date]]];
    
    [postParams setObject:date_time forKey:@"date_time"];
    
    NSData *postData = [Utils encodeDictionary:postParams];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        
        [CustomLoading DismissAlertMessage];
        
        NSString* newStr = [NSString stringWithUTF8String:[data bytes]];
        
        
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int flag = [[result objectForKey:@"status"] intValue];
            
            if(flag == 1) {
                
                User *user = [[User alloc] initWithDictionary:[result objectForKey:@"user_data"]];
                [user populateEvents:(NSArray*)[result objectForKey:@"events"]];
                [user populateBookedCallsTime:(NSArray*)[result objectForKey:@"user_specific_booked_calls"]];
                [user populateZodiacStarsArray:(NSArray*)[result objectForKey:@"hoscopesro"]];
                [user populateNewsArray:(NSArray*)[result objectForKey:@"news"]];
                [user populatePicturesArray:(NSArray*)[result objectForKey:@"pictures"]];
                [user populateWorkingHoursArray:(NSArray*)[result objectForKey:@"working_hours"]];
                [user populateVideoArray:(NSArray*)[result objectForKey:@"videos"]];
                [user populateTransactionArray:(NSArray*)[result objectForKey:@"transactions_history"]];
                CreditCard *ccObj = [[CreditCard alloc] initWithDictionary:[result objectForKey:@"credit_card_info"]];
                user.creditCardObj = ccObj;
                
                AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
                appDel.sharedUserModel = user;
                
                [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"isLoggedIn"];
                [[NSUserDefaults standardUserDefaults] setObject:user.userID forKey:@"userID"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                HomeVC *homeVC = [[HomeVC alloc] initWithNibName:@"HomeVC" bundle:nil];
                [self.navigationController pushViewController:homeVC animated:YES];
                
            }
            else {
                
                [self presentViewController:[Utils showCustomAlert:@"حدث خطأ ما" andMessage:[result objectForKey:@"message"]] animated:YES completion:nil];
                
            }
        }
        else{
            
            [self presentViewController:[Utils showCustomAlert:@"Conection Error" andMessage:@"Please retry after some time"] animated:YES completion:nil];
        }
    }];
}
- (IBAction)logInOptionPressed:(id)sender {
    
    NSArray * arr = [[NSArray alloc] init];
    arr = [NSArray arrayWithObjects:@"تسجيل الدخول عبر البريد الإلكتروني", @"تسجيل الدخول عن طريق رقم الهاتف",nil];
    NSArray * arrImage = [[NSArray alloc] init];
    arrImage = [NSArray arrayWithObjects:[UIImage imageNamed:@""], [UIImage imageNamed:@""], nil];
    if(dropDown == nil) {
        CGFloat f = 186;
        if(IS_IPAD) {
            f = 280;
        }
        dropDown = [[NIDropDown alloc]showDropDown:sender :&f :arr :arrImage :@"down"];
        dropDown.delegate = self;
    }
    else {
        [dropDown hideDropDown:sender];
        [self rel];
    }
    
    
}


-(void)rel{
    //    [dropDown release];
    dropDown = nil;
}

- (void) niDropDownDelegateMethod: (NIDropDown *) sender {
    if(sender.selectedIndex == 0) {
        isLoginFromNumber= false;
        optionBtn.titleLabel.text = @"تسجيل الدخول عبر البريد الإلكتروني";
        
        _email.keyboardType = UIKeyboardTypeEmailAddress;
        _email.placeholder = @"البريد الإلكتروني";
    }
    else if(sender.selectedIndex == 1) {
        isLoginFromNumber= true;
        optionBtn.titleLabel.text = @"تسجيل الدخول عن طريق رقم الهاتف";
        
        _email.keyboardType = UIKeyboardTypeNumberPad;
        _email.placeholder = @"رقم الهاتف";
        
    }
    [self rel];
}
@end

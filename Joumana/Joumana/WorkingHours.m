//
//  WorkingHours.m
//  Joumana
//
//  Created by Ahmed Sadiq on 18/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "WorkingHours.h"
#import "NSString+Date.h"

@implementation WorkingHours

- (id)initWithDictionary:(NSDictionary *) responseData
{
    
    self = [super init];
    
    if (self) {
        
        self.day = [self validStringForObject:responseData [@"day"]];
        self.end_time  = [self validStringForObject:responseData [@"end_time"]];
        self.start_time  = [self validStringForObject:responseData [@"start_time"]];
        self.timesArray = [[NSMutableArray alloc] init];
        NSArray *tempTimeArray = [responseData objectForKey:@"time"];
        for (int i = 0; i< tempTimeArray.count; i++){
            NSDictionary *dictTemp = (NSDictionary*) [tempTimeArray objectAtIndex:i];
            NSString *endTime = [self convertDateINtoLocalTimezoneWithFormate:@"hh:mm" andDate:[dictTemp objectForKey:@"start_time"]];
            [self.timesArray addObject:endTime];
        }
    }
    
    return self;
}
-(NSString *) convertDateINtoLocalTimezoneWithFormate:(NSString *)formate andDate:(NSString *)dateStr{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    
    NSDate *date = [dateFormatter dateFromString:dateStr]; // create date from string
    
    // change to a readable time format and change to local time zone
    [dateFormatter setDateFormat:formate];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    NSString *timestamp = [dateFormatter stringFromDate:date];
    return timestamp;
}


@end

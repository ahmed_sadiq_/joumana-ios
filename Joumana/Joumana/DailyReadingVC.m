//
//  DailyReadingVC.m
//  Joumana
//
//  Created by Ahmed Sadiq on 23/09/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "DailyReadingVC.h"
#import "DailyReadingDetailVC.h"
#import "ZodiacStar.h"

@interface DailyReadingVC ()

@end

@implementation DailyReadingVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    delegate = (AppDelegate*) [UIApplication sharedApplication].delegate;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)menuPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)starPressed:(id)sender {
    
    UIButton *btnSender = (UIButton*)sender;
    
    ZodiacStar *zStar = [delegate.sharedUserModel.zodiacStarsArray objectAtIndex:(btnSender.tag)-1];
    
    
    
    DailyReadingDetailVC *dailyReadingDetail = [[DailyReadingDetailVC alloc] initWithNibName:@"DailyReadingDetailVC" bundle:nil];
    
    dailyReadingDetail.currentStar = zStar;
    
    [self.navigationController pushViewController:dailyReadingDetail animated:YES];
    [self.navigationController setNavigationBarHidden:YES];
}
@end

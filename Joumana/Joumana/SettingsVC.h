//
//  SettingsVC.h
//  Joumana
//
//  Created by Ahmed Sadiq on 25/09/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"
#import "AppDelegate.h"

@interface SettingsVC : UIViewController<UITableViewDelegate,UITableViewDataSource> {
    AppDelegate *delegate;
    
}
@property (strong, nonatomic) UIRefreshControl *refreshControl;
@property (strong, nonatomic) IBOutlet AsyncImageView *imgView;

@property (strong, nonatomic) IBOutlet UILabel *titleLbl;
@property (strong, nonatomic) IBOutlet UILabel *dateLbl;

@property (strong, nonatomic) IBOutlet UIButton *bookingBtn;
@property (strong, nonatomic) IBOutlet UIButton *settingsBtn;

@property(nonatomic,weak) IBOutlet UITableView * expandTableView;

@property(nonatomic,strong) NSMutableArray * dataModelArray;
@property ( strong , nonatomic ) UINavigationController *navController;

@property (strong, nonatomic) IBOutlet UITableView *lowerTblView;


- (IBAction)bookingPressed:(id)sender;
- (IBAction)settingsPressed:(id)sender;


@end

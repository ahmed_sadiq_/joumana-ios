//
//  News.h
//  Joumana
//
//  Created by Ahmed Sadiq on 18/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseEntity.h"

@interface News : BaseEntity

@property (nonatomic, strong) NSString * date_time;
@property (nonatomic, strong) NSString * detail;
@property (nonatomic, strong) NSString * favourite;
@property (nonatomic, strong) NSString * news_id;
@property (nonatomic, strong) NSString * picture;
@property (nonatomic, strong) NSString * title;

- (id)initWithDictionary:(NSDictionary *) responseData;

@end

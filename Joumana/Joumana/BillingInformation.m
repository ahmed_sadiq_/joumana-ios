//
//  BillingInformation.m
//  Joumana
//
//  Created by Ahmed Sadiq on 24/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "BillingInformation.h"
#import "CustomLoading.h"
#import "AppDelegate.h"
#import "CreditCard.h"
#import "Constants.h"
#import "Utils.h"
#import "User.h"
@interface BillingInformation ()

@end

@implementation BillingInformation

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.cardNum setValue:[UIColor whiteColor]
              forKeyPath:@"_placeholderLabel.textColor"];
    [self.expiryNum setValue:[UIColor whiteColor]
             forKeyPath:@"_placeholderLabel.textColor"];
    [self.cvcNum setValue:[UIColor whiteColor]
                  forKeyPath:@"_placeholderLabel.textColor"];
    AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
    
    if(appDel.sharedUserModel.creditCardObj) {
        self.cardNum.text = appDel.sharedUserModel.creditCardObj.card_number;
        self.expiryNum.text = [NSString stringWithFormat:@"%@/%@",appDel.sharedUserModel.creditCardObj.expiry_month,appDel.sharedUserModel.creditCardObj.expiry_year];
        self.cvcNum.text = appDel.sharedUserModel.creditCardObj.cvc;
    }
    [self.submitBtn.layer setBorderColor:[UIColor colorWithRed:0.816 green:(0.658) blue:(0.384) alpha:1.0].CGColor];
    self.submitBtn.layer.borderWidth = 1.5f;
    self.submitBtn.layer.masksToBounds = true;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Text Field Delegate Methods
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    [self animateTextField:nil up:YES];
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField:nil up:NO];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const int movementDistance = 145; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}

- (IBAction)dobValueChanged:(id)sender {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM/yyyy"];
    _expiryNum.text = [NSString stringWithFormat:@"%@", [dateFormatter stringFromDate: _expiryPicker.date]];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (BOOL) validateFields {
    if(_cardNum.text.length < 2 || _expiryNum.text.length < 2 || _cvcNum.text.length < 2) {
        return false;
    }
    return true;
}

- (IBAction)submitPressed:(id)sender {
    NSString *userID = (NSString*)[[NSUserDefaults standardUserDefaults] objectForKey:@"userID"];
    
    if([self validateFields]) {
        [CustomLoading showAlertMessage];
        
        NSString *urlStr = [NSString stringWithFormat:@"%@%@",BASE_URL,@"Transaction/add_credit_card_info"];
        NSURL *url = [NSURL URLWithString:urlStr];
        
        NSString *monthStr = [[_expiryNum.text componentsSeparatedByString:@"/"] objectAtIndex:0];
        NSString *yearStr = [[_expiryNum.text componentsSeparatedByString:@"/"] objectAtIndex:1];
        
        NSMutableDictionary *postParams = [[NSMutableDictionary alloc] init];
        [postParams setObject:userID forKey:@"user_id"];
        [postParams setObject:_cardNum.text forKey:@"card_number"];
        [postParams setObject:monthStr forKey:@"expiry_month"];
        [postParams setObject:yearStr forKey:@"expiry_year"];
        [postParams setObject:_cvcNum.text forKey:@"cvc"];
        
        NSData *postData = [Utils encodeDictionary:postParams];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:url];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:postData];
        
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
            
            [CustomLoading DismissAlertMessage];
            if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
            {
                NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                int flag = [[result objectForKey:@"status"] intValue];
                
                if(flag == 1) {
                    
                    CreditCard *ccObj = [[CreditCard alloc] initWithDictionary:[result objectForKey:@"credit_card_info"]];
                    
                    AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
                    appDel.sharedUserModel.creditCardObj = ccObj;
                }
                else {
                    
                    [self presentViewController:[Utils showCustomAlert:@"حدث خطأ ما" andMessage:[result objectForKey:@"message"]] animated:YES completion:nil];
                    
                }
            }
            else{
                
                [self presentViewController:[Utils showCustomAlert:@"Conection Error" andMessage:@"Please retry after some time"] animated:YES completion:nil];
            }
        }];
    }
    
}

- (IBAction)backToLoginPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

- (IBAction)expiryDatePressed:(id)sender {
    _expiryView.hidden = false;
}

- (IBAction)expiryDonePressed:(id)sender {
    _expiryView.hidden = true;
}
- (IBAction)backPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}
@end

//
//  StarsCollectionView.h
//  Joumana
//
//  Created by Ahmed Sadiq on 22/12/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StarsCollectionView : UIViewController
@property int option;
@property (nonatomic,assign) BOOL isFromYearlyExpectation;

@property (strong, nonatomic) IBOutlet UILabel *titleLbl;
@property (strong, nonatomic) NSMutableArray *datesCollection;
- (IBAction)starPressed:(id)sender;
- (IBAction)backPressed:(id)sender;

@end

//
//  News.m
//  Joumana
//
//  Created by Ahmed Sadiq on 18/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "News.h"

@implementation News

- (id)initWithDictionary:(NSDictionary *) responseData
{
    
    self = [super init];
    
    if (self) {
        
        self.date_time = [self validStringForObject:responseData [@"date_time"]];
        self.detail = [self validStringForObject:responseData [@"detail"]];
        self.favourite = [self validStringForObject:responseData [@"favourite"]];
        self.news_id = [self validStringForObject:responseData [@"news_id"]];
        self.picture  = [self completeImageURL:responseData [@"picture"]];
        self.title  = [self validStringForObject:responseData [@"title"]];
        
    }
    
    return self;
}

@end

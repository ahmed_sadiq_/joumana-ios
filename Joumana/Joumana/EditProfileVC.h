//
//  EditProfileVC.h
//  Joumana
//
//  Created by Ahmed Sadiq on 24/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"
#import "AppDelegate.h"
#import <QuartzCore/QuartzCore.h>

@interface EditProfileVC : UIViewController <UIActionSheetDelegate,UIImagePickerControllerDelegate,UITextFieldDelegate> {
    AppDelegate *delegate;
    
}

@property (strong, nonatomic) IBOutlet UIView *DOBVIEW;
@property (strong, nonatomic) IBOutlet UIButton *submitBtn;
@property (strong, nonatomic) IBOutlet UITextField *nameTxt;
@property (strong, nonatomic) IBOutlet UITextField *bdayTxt;
@property (strong, nonatomic) IBOutlet UITextField *emailTxt;
@property (strong, nonatomic) IBOutlet UIDatePicker *dobPicker;
@property (strong, nonatomic) IBOutlet AsyncImageView *imgView;



- (IBAction)dobDonePressed:(id)sender;
- (IBAction)birthdayPressed:(id)sender;
- (IBAction)submitPressed:(id)sender;
- (IBAction)backPressed:(id)sender;
- (IBAction)imgchangeRequestPressed:(id)sender;
@end

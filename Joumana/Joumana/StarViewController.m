//
//  StarViewController.m
//  Joumana
//
//  Created by Ahmed Sadiq on 22/12/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "StarViewController.h"
#import "InternalTableView.h"
#import "CustomLoading.h"
#import <Social/Social.h>
#import "PostModel.h"
#import "DateCell.h"
#import "Utils.h"
#import "DSLCalendarView.h"


@interface StarViewController ()<FSCalendarDataSource, FSCalendarDelegate>
{
    NSString *selectedDate;

}
@end

@implementation StarViewController


-(void) setupCalender{
    self.gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    // border
    [self.calendarView.layer setBorderColor:[UIColor colorWithRed:0.816 green:(0.658) blue:(0.384) alpha:1.0].CGColor];
    [self.calendarView.layer setBorderWidth:1.5f];
    self.calendarView.layer.masksToBounds = true;
    
    self.dateFormatter = [[NSDateFormatter alloc] init];
    self.dateFormatter.dateFormat = @"yyyy-MM-dd";
    
    
    // 450 for iPad and 300 for iPhone
    CGFloat height = self.calendarView.frame.size.height;
    FSCalendar *calendar = [[FSCalendar alloc] initWithFrame:CGRectMake(0, 0, self.calendarView.frame.size.width, height)];
    calendar.dataSource = self;
    calendar.delegate = self;
    calendar.backgroundColor = [UIColor clearColor];
    calendar.appearance.headerMinimumDissolvedAlpha = 0;
    
    calendar.appearance.caseOptions = FSCalendarCaseOptionsHeaderUsesUpperCase;
    [self.calendarView addSubview:calendar];
    self.calendar = calendar;
    
    UIButton *previousButton = [UIButton buttonWithType:UIButtonTypeCustom];
    previousButton.frame = CGRectMake(-20, 5, 95, 34);
    previousButton.backgroundColor = [UIColor clearColor];
    previousButton.titleLabel.font = [UIFont systemFontOfSize:15];
    [previousButton setImage:[UIImage imageNamed:@"icon_prev"] forState:UIControlStateNormal];
    [previousButton addTarget:self action:@selector(previousClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.calendarView addSubview:previousButton];
    self.previousButton = previousButton;
    
    UIButton *nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
    nextButton.frame = CGRectMake(CGRectGetWidth(self.calendarView.frame)-75, 5, 95, 34);
    nextButton.backgroundColor = [UIColor clearColor];
    nextButton.titleLabel.font = [UIFont systemFontOfSize:15];
    [nextButton setImage:[UIImage imageNamed:@"icon_next"] forState:UIControlStateNormal];
    [nextButton addTarget:self action:@selector(nextClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.calendarView addSubview:nextButton];
    self.nextButton = nextButton;
    
    

    


}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setupCalender];
    _starName.text = _currentStar.starName;
    
    NSString *starName = @"";
    
    switch ([_currentStar.starId intValue]) {
            
        case 1:
            starName = @"aries";
            break;
        case 2:
            starName = @"taurus";
            break;
        case 3:
            starName = @"gemini";
            break;
        case 4:
            starName = @"cancer";
            break;
        case 5:
            starName = @"leo";
            break;
        case 6:
            starName = @"virgo";
            break;
        case 7:
            starName = @"libra";
            break;
        case 8:
            starName = @"scorpio";
            break;
        case 9:
            starName = @"sagittarius";
            break;
        case 10:
            starName = @"capricorn";
            break;
        case 11:
            starName = @"aquarius";
            break;
        case 12:
            starName = @"pisces";
            break;
            
        default:
            break;
    }
    
    NSString *iconName = [NSString stringWithFormat:@"icon%@_white",starName];
    // iconName=[iconName lowercaseString];
    _starImg.image = [UIImage imageNamed:iconName];
    
    if(_option == 1) {
        _titleLbl.text = @"القراءات اليومية";
    }
    else if(_option == 2) {
        _titleLbl.text = @"القراءات الأسبوعية";
    }
    else if(_option == 3) {
        _titleLbl.text = @"أرقام الحظ";
    }
    else if(_option == 10){
        _titleLbl.text = @"الأبراج اليومية";
        
    }
    else {
        _titleLbl.text = @"نصيحة";
    }
    if(_datesArray.count == 0) {
        _noDataLbl.hidden = false;
    }
    
}

-(void)viewWillAppear:(BOOL)animated{
   // [self.calendar reloadData];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)btnHideCalender:(id)sender {
    
    _boxView.hidden = YES;
    
}
- (IBAction)btnToday:(id)sender {
    NSDate *todayDate = [NSDate date]; //Get todays date
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init]; // here we create NSDateFormatter object for change the Format of date.
    [dateFormatter setDateFormat:@"yyyy-MM-dd"]; //Here we can set the format which we need
    NSString *convertedDateString = [dateFormatter stringFromDate:todayDate];
    [self getDailyPost:convertedDateString];

    
}
- (IBAction)btnYesterday:(id)sender {
    NSDate *today = [NSDate date];

    NSDate *yesterday = [today dateByAddingTimeInterval: -86400.0];

    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init]; // here we create NSDateFormatter object for change the Format of date.
    [dateFormatter setDateFormat:@"yyyy-MM-dd"]; //Here we can set the format which we need
    NSString *convertedDateString = [dateFormatter stringFromDate:yesterday];
    [self getDailyPost:convertedDateString];

}
- (IBAction)btnMore:(id)sender {
    _boxView.hidden = NO;
}


- (IBAction)instaPressed:(id)sender {
    UIImage *imageToShare = [self capture];
    
    NSURL *instagramURL = [NSURL URLWithString:@"instagram://app"];
    if ([[UIApplication sharedApplication] canOpenURL:instagramURL])
    {
        UIImage *imageToUse = imageToShare;
        NSString *documentDirectory=[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
        NSString *saveImagePath=[documentDirectory stringByAppendingPathComponent:@"Image.igo"];
        NSData *imageData=UIImagePNGRepresentation(imageToUse);
        [imageData writeToFile:saveImagePath atomically:YES];
        NSURL *imageURL=[NSURL fileURLWithPath:saveImagePath];
        self.documentController=[[UIDocumentInteractionController alloc]init];
        self.documentController = [UIDocumentInteractionController interactionControllerWithURL:imageURL];
        self.documentController.delegate = self;
        self.documentController.annotation = [NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"Testing"], @"InstagramCaption", nil];
        self.documentController.UTI = @"com.instagram.exclusivegram";
        UIViewController *vc = [UIApplication sharedApplication].keyWindow.rootViewController;
        [self.documentController presentOpenInMenuFromRect:CGRectMake(1, 1, 1, 1) inView:vc.view animated:YES];
    }
    else
    {
        NSLog (@"Instagram not found");
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:@"Account Configuration Error"
                                              message:@"Please install instagram application from appstore"
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                       NSLog(@"OK action");
                                   }];
        
        [alertController addAction:okAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
        
    }
}

- (IBAction)fbPressed:(id)sender {
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        
        
        [controller setInitialText:@"برجي اليومي من جومانا الكبيسي"];
        [controller addImage:[self capture]];
        
        [self presentViewController:controller animated:YES completion:Nil];
        
        SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
            if (result == SLComposeViewControllerResultCancelled) {
                
                NSLog(@"delete");
                
            } else
                
            {
                NSLog(@"post");
            }
        };
        controller.completionHandler =myBlock;
        
        
    }else {
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:@"Account Configuration Error"
                                              message:@"Please configure Facebook Account in Settings. "
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                       NSLog(@"OK action");
                                   }];
        
        [alertController addAction:okAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

- (IBAction)twitterPressed:(id)sender {
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        SLComposeViewController *tweetSheet = [SLComposeViewController
                                               composeViewControllerForServiceType:SLServiceTypeTwitter];
        [tweetSheet setInitialText:@"برجي اليومي من جومانا الكبيسي"];
        [tweetSheet addImage:[self capture]];
        [self presentViewController:tweetSheet animated:YES completion:nil];
        
        SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
            if (result == SLComposeViewControllerResultCancelled) {
                NSLog(@"delete");
            } else
            {
                NSLog(@"post");
            }
            //   [composeController dismissViewControllerAnimated:YES completion:Nil];
        };
        tweetSheet.completionHandler =myBlock;
    }
    else {
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:@"Account Configuration Error"
                                              message:@"Please configure Twitter Account in Settings. "
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                       NSLog(@"OK action");
                                   }];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

- (IBAction)backPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return _datesArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *simpleTableIdentifier = @"DateCell";
    DateCell *cell = (DateCell *)[_dateTblView dequeueReusableCellWithIdentifier:nil];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"DateCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    cell.dateLbl.text = [_datesArray objectAtIndex:indexPath.row];;
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 34;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [CustomLoading showAlertMessage];
    
    NSString *strSecondPart = [NSString stringWithFormat:@"Post/get_posts?type=%@&star_id=%@&page_no=1&date=%@",[NSString stringWithFormat:@"%d",_option],_currentStar.starId,[_datesArray objectAtIndex:indexPath.row]];
    NSString *urlStr = [NSString stringWithFormat:@"%@%@",BASE_URL,strSecondPart];
    NSURL *url = [NSURL URLWithString:urlStr];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"GET"];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        
        [CustomLoading DismissAlertMessage];
        
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int flag = [[result objectForKey:@"status"] intValue];
            
            if(flag == 1) {
                self.postArray = [[NSMutableArray alloc] init];
                NSArray *postArray = [result objectForKey:@"posts"];
                for(int i=0; i<postArray.count; i++) {
                    NSDictionary *postDict = [postArray objectAtIndex:i];
                    [self.postArray addObject:[[PostModel alloc] initWithDictionary:postDict]];
                }
                
                
                InternalTableView *starViewCont = [[InternalTableView alloc] initWithNibName:@"InternalTableView" bundle:nil];
                starViewCont.postArray = self.postArray;
                starViewCont.option = _option;
                [self.navigationController pushViewController:starViewCont animated:YES];
                [self.navigationController setNavigationBarHidden:YES];
                
            }
            else {
                
                [self presentViewController:[Utils showCustomAlert:@"حدث خطأ ما" andMessage:[result objectForKey:@"message"]] animated:YES completion:nil];
            }
        }
        else{
            
            [self presentViewController:[Utils showCustomAlert:@"Conection Error" andMessage:@"Please retry after some time"] animated:YES completion:nil];
        }
    }];
    
}
-(UIImage *)capture{
    
    UIGraphicsBeginImageContext(self.view.bounds.size);
    [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *imageView = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    UIImageWriteToSavedPhotosAlbum(imageView, nil, nil, nil); //if you need to save
    return imageView;
    
}

-(void) getDailyPost:(NSString *)date{

    [CustomLoading showAlertMessage];
    
    NSString *strSecondPart = [NSString stringWithFormat:@"Post/get_posts?type=%@&star_id=%@&page_no=1&date=%@",[NSString stringWithFormat:@"%d",_option],_currentStar.starId,date];
    NSString *urlStr = [NSString stringWithFormat:@"%@%@",BASE_URL,strSecondPart];
    NSURL *url = [NSURL URLWithString:urlStr];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"GET"];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        
        [CustomLoading DismissAlertMessage];
        
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int flag = [[result objectForKey:@"status"] intValue];
            
            if(flag == 1) {
                self.postArray = [[NSMutableArray alloc] init];
                NSArray *postArray = [result objectForKey:@"posts"];
                for(int i=0; i<postArray.count; i++) {
                    NSDictionary *postDict = [postArray objectAtIndex:i];
                    [self.postArray addObject:[[PostModel alloc] initWithDictionary:postDict]];
                }
                
                _boxView.hidden = YES;
                InternalTableView *starViewCont = [[InternalTableView alloc] initWithNibName:@"InternalTableView" bundle:nil];
                starViewCont.postArray = self.postArray;
                starViewCont.option = _option;
                [self.navigationController pushViewController:starViewCont animated:YES];
                [self.navigationController setNavigationBarHidden:YES];
                
            }
            else {
                
                [self presentViewController:[Utils showCustomAlert:@"حدث خطأ ما" andMessage:[result objectForKey:@"message"]] animated:YES completion:nil];
            }
        }
        else{
            
            [self presentViewController:[Utils showCustomAlert:@"Conection Error" andMessage:@"Please retry after some time"] animated:YES completion:nil];
        }
    }];


}




- (void)previousClicked:(id)sender
{
    NSDate *currentMonth = self.calendar.currentPage;
    NSDate *previousMonth = [self.gregorian dateByAddingUnit:NSCalendarUnitMonth value:-1 toDate:currentMonth options:0];
    [self.calendar setCurrentPage:previousMonth animated:YES];
}

- (void)nextClicked:(id)sender
{
    NSDate *currentMonth = self.calendar.currentPage;
    NSDate *nextMonth = [self.gregorian dateByAddingUnit:NSCalendarUnitMonth value:1 toDate:currentMonth options:0];
    [self.calendar setCurrentPage:nextMonth animated:YES];
}


#pragma mark - DSLCalendarViewDelegate methods

- (void)calendarView:(DSLCalendarView *)calendarView didSelectRange:(DSLCalendarRange *)range {
    if (range != nil) {
        NSLog( @"Selected %ld/%ld - %ld/%ld", (long)range.startDay.day, (long)range.startDay.month, (long)range.endDay.day, (long)range.endDay.month);
    }
    else {
        NSLog( @"No selection" );
    }
}

- (DSLCalendarRange*)calendarView:(DSLCalendarView *)calendarView didDragToDay:(NSDateComponents *)day selectingRange:(DSLCalendarRange *)range {
    if (NO) { // Only select a single day
        return [[DSLCalendarRange alloc] initWithStartDay:day endDay:day];
    }
    else if (YES) { // Don't allow selections before today
        NSDateComponents *today = [[NSDate date] dslCalendarView_dayWithCalendar:calendarView.visibleMonth.calendar];
        
        NSDateComponents *startDate = range.startDay;
        NSDateComponents *endDate = range.endDay;
        
        if ([self day:startDate isBeforeDay:today] && [self day:endDate isBeforeDay:today]) {
            return nil;
        }
        else {
            if ([self day:startDate isBeforeDay:today]) {
                startDate = [today copy];
            }
            if ([self day:endDate isBeforeDay:today]) {
                endDate = [today copy];
            }
            
            return [[DSLCalendarRange alloc] initWithStartDay:startDate endDay:endDate];
        }
    }
    
    return range;
}

- (void)calendarView:(DSLCalendarView *)calendarView willChangeToVisibleMonth:(NSDateComponents *)month duration:(NSTimeInterval)duration {
    NSLog(@"Will show %@ in %.3f seconds", month, duration);
}

- (void)calendarView:(DSLCalendarView *)calendarView didChangeToVisibleMonth:(NSDateComponents *)month {
    NSLog(@"Now showing %@", month);
}

- (BOOL)day:(NSDateComponents*)day1 isBeforeDay:(NSDateComponents*)day2 {
    return ([day1.date compare:day2.date] == NSOrderedAscending);
}

#pragma mark - FSCalendarDelegate

- (void)calendar:(FSCalendar *)calendar didSelectDate:(NSDate *)date
{
    
    selectedDate = [self.dateFormatter stringFromDate:date];
    [self getDailyPost:selectedDate];
}

- (void)calendarCurrentPageDidChange:(FSCalendar *)calendar
{
    //NSLog(@"did change page %@",[self.dateFormatter stringFromDate:calendar.currentPage]);
}


@end

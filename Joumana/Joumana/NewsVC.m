//
//  NewsVC.m
//  Joumana
//
//  Created by Ahmed Sadiq on 21/09/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "NewsVC.h"
#import "NewsCell.h"
#import "News.h"
#import "Utils.h"
#import "Events.h"
#import "CustomLoading.h"
#import <QuartzCore/QuartzCore.h>
@interface NewsVC ()

@end

@implementation NewsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    _detailView.hidden = true;
    _eventView.hidden = true;
    
    [self populateUpperScroll];
    
}

- (void) populateUpperScroll {
    int x = 15;
    int y = 0;
    
    for(int i=0; i<delegate.sharedUserModel.newsArray.count; i++) {
        News *nModel = (News*)[delegate.sharedUserModel.newsArray objectAtIndex:i];
        
        UIView *backgroundView = [[UIView alloc] initWithFrame:CGRectMake(x, y, 300, 288)];
        backgroundView.backgroundColor = [UIColor colorWithRed:213/255.0f green:201/255.0f blue:188/255.0f alpha:1.0];
        
        AsyncImageView *imgView = [[AsyncImageView alloc] initWithFrame:CGRectMake(0, 0, 300, 183)];
        imgView.image = [UIImage imageNamed:@"placeholder"];
        imgView.imageURL = [NSURL URLWithString:nModel.picture];
        NSURL *url = [NSURL URLWithString:nModel.picture];
        [[AsyncImageLoader sharedLoader] loadImageWithURL:url];
        
        
        UIButton *readMoreBtn = [[UIButton alloc] initWithFrame:CGRectMake(15, 139, 85, 35)];
        readMoreBtn.layer.cornerRadius = 5.5;
        readMoreBtn.layer.borderWidth = 0;
        [readMoreBtn setBackgroundColor:[UIColor colorWithRed:197/255.0 green:170/255.0 blue:116/255.0 alpha:1.0]];
        [readMoreBtn setTitle:@"اقرأ أكثر" forState:UIControlStateNormal];
        [readMoreBtn addTarget:self action:@selector(readMorePressed:) forControlEvents:UIControlEventTouchUpInside];
        readMoreBtn.tag = i;
        
        UILabel *titleLbl = [[UILabel alloc] initWithFrame:CGRectMake(5, 185, 290, 44)];
        titleLbl.text = nModel.title;
        titleLbl.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:16.0f];
        titleLbl.textColor = [UIColor colorWithRed:0.337 green:0.356 blue:0.494 alpha:1.0];
        titleLbl.textAlignment = UITextAlignmentRight;
        titleLbl.numberOfLines = 3;
        titleLbl.minimumScaleFactor = 0.5;
        
        UITextView *txtView = [[UITextView alloc] initWithFrame:CGRectMake(0 , 229, 300, 63)];
        txtView.editable = false;
        txtView.text = nModel.detail;
        txtView.textColor = [UIColor colorWithRed:0.576 green:0.588 blue:0.619 alpha:1.0];
        txtView.font = [UIFont fontWithName:@"HelveticaNeue" size:12.0f];
        txtView.textAlignment = UITextAlignmentRight;
        [txtView setBackgroundColor:[UIColor clearColor]];
        [backgroundView addSubview:imgView];
        [backgroundView addSubview:readMoreBtn];
        [backgroundView addSubview:titleLbl];
        [backgroundView addSubview:txtView];
        
        [_upperScrollView addSubview:backgroundView];
        x=x+310;
    }
    _pager.numberOfPages = delegate.sharedUserModel.newsArray.count;
    _upperScrollView.contentSize = CGSizeMake(delegate.sharedUserModel.newsArray.count*320, 200);
    
}

- (void) populateUpperScrollForFavourite {
    int x = 15;
    int y = 0;
    
    for(int i=0; i<favouriteArray.count; i++) {
        News *nModel = (News*)[favouriteArray objectAtIndex:i];
        
        UIView *backgroundView;
        if(favouriteArray.count == 1) {
            backgroundView = [[UIView alloc] initWithFrame:CGRectMake(37, y, 300, 288)];
        }
        else {
            backgroundView = [[UIView alloc] initWithFrame:CGRectMake(x, y, 300, 288)];
        }
        
        backgroundView.backgroundColor = [UIColor colorWithRed:213/255.0f green:201/255.0f blue:188/255.0f alpha:1.0];
        
        AsyncImageView *imgView = [[AsyncImageView alloc] initWithFrame:CGRectMake(0, 0, 300, 183)];
        imgView.image = [UIImage imageNamed:@"placeholder"];
        imgView.imageURL = [NSURL URLWithString:nModel.picture];
        NSURL *url = [NSURL URLWithString:nModel.picture];
        [[AsyncImageLoader sharedLoader] loadImageWithURL:url];
        
        UIButton *readMoreBtn = [[UIButton alloc] initWithFrame:CGRectMake(15, 139, 85, 35)];
        readMoreBtn.layer.cornerRadius = 5.5;
        readMoreBtn.layer.borderWidth = 0;
        [readMoreBtn setBackgroundColor:[UIColor colorWithRed:197/255.0 green:170/255.0 blue:116/255.0 alpha:1.0]];
        [readMoreBtn setTitle:@"اقرأ أكثر" forState:UIControlStateNormal];
        [readMoreBtn addTarget:self action:@selector(readMorePressedForFavourite:) forControlEvents:UIControlEventTouchUpInside];
        readMoreBtn.tag = i;

        
        UILabel *titleLbl = [[UILabel alloc] initWithFrame:CGRectMake(5, 185, 290, 44)];
        titleLbl.text = nModel.title;
        titleLbl.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:16.0f];
        titleLbl.textColor = [UIColor colorWithRed:0.337 green:0.356 blue:0.494 alpha:1.0];
        titleLbl.textAlignment = UITextAlignmentRight;
        titleLbl.numberOfLines = 3;
        titleLbl.minimumScaleFactor = 0.5;
        
        UITextView *txtView = [[UITextView alloc] initWithFrame:CGRectMake(0 , 222, 300, 63)];
        txtView.editable = false;
        txtView.text = nModel.detail;
        [txtView setBackgroundColor:[UIColor clearColor]];
        txtView.textColor = [UIColor colorWithRed:0.576 green:0.588 blue:0.619 alpha:1.0];
        txtView.textAlignment = UITextAlignmentRight;
        txtView.font = [UIFont fontWithName:@"HelveticaNeue" size:12.0f];
        
        [backgroundView addSubview:imgView];
        [backgroundView addSubview:readMoreBtn];
        [backgroundView addSubview:titleLbl];
        [backgroundView addSubview:txtView];
        
        [_upperScrollView addSubview:backgroundView];
        x=x+310;
    }
    
    _pager.numberOfPages = favouriteArray.count;
    _upperScrollView.contentSize = CGSizeMake(favouriteArray.count*320, 200);
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return delegate.sharedUserModel.newsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *simpleTableIdentifier = @"NewsCell";
    NewsCell *cell = (NewsCell *)[_eventTblView dequeueReusableCellWithIdentifier:nil];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"NewsCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    News *eventObj = (News*)[delegate.sharedUserModel.newsArray objectAtIndex:indexPath.row];
    
    cell.eventTxt.text = eventObj.detail;
    cell.lblTitle.text = eventObj.title;

    cell.imgView.image = [UIImage imageNamed:@"placeholder"];
    cell.imgView.imageURL = [NSURL URLWithString:eventObj.picture];
    NSURL *url = [NSURL URLWithString:eventObj.picture];
    [[AsyncImageLoader sharedLoader] loadImageWithURL:url];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 90;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    News *nModel = (News*)[delegate.sharedUserModel.newsArray objectAtIndex:indexPath.row];
    
    _detailView.hidden = false;
    selectedNewsId = nModel.news_id;
    
    _detailImgView.image = [UIImage imageNamed:@"placeholder"];
    _detailImgView.imageURL = [NSURL URLWithString:nModel.picture];
    NSURL *url = [NSURL URLWithString:nModel.picture];
    [[AsyncImageLoader sharedLoader] loadImageWithURL:url];
    
    _detailTitleLbl.text = nModel.title;
    _detailTxt.text = nModel.detail;
    BOOL isFav = [nModel.favourite boolValue];
    
    if(isFav) {
        [_startBtn setImage:[UIImage imageNamed:@"star_filled"] forState:UIControlStateNormal];
    }
    else {
        [_startBtn setImage:[UIImage imageNamed:@"star_unfilled"] forState:UIControlStateNormal];
    }
}

- (IBAction)menuPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

- (IBAction)detailStarPressed:(id)sender {
    
    News *nModel;
    
    for(int i=0; i<delegate.sharedUserModel.newsArray.count; i++) {
        nModel = [delegate.sharedUserModel.newsArray objectAtIndex:i];
        
        if([nModel.news_id isEqualToString:selectedNewsId]) {
            break;
        }
        
        
    }
    
    BOOL isFav = [nModel.favourite boolValue];
    
    if(isFav) {
        [_startBtn setImage:[UIImage imageNamed:@"star_unfilled"] forState:UIControlStateNormal];
    }
    else {
        [_startBtn setImage:[UIImage imageNamed:@"star_filled"] forState:UIControlStateNormal];
    }
    
    [CustomLoading showAlertMessage];
    
    NSString *urlStr = [NSString stringWithFormat:@"%@%@",BASE_URL,@"News/favourite/"];
    NSURL *url = [NSURL URLWithString:urlStr];
    
    NSMutableDictionary *postParams = [[NSMutableDictionary alloc] init];
    [postParams setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"userID"] forKey:@"user_id"];
    [postParams setObject:selectedNewsId forKey:@"news_id"];
    
    NSData *postData = [Utils encodeDictionary:postParams];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response , NSData  *data, NSError *error) {
        
        [CustomLoading DismissAlertMessage];
        
        if ( [(NSHTTPURLResponse *)response statusCode] == 200 )
        {
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            int flag = [[result objectForKey:@"status"] intValue];
            
            if(flag == 1) {
                BOOL isFav = [nModel.favourite boolValue];
                if(isFav) {
                    nModel.favourite = @"0";
                }
                else {
                    nModel.favourite = @"1";
                }
            }
            else {
                
                [self presentViewController:[Utils showCustomAlert:@"حدث خطأ ما" andMessage:[result objectForKey:@"message"]] animated:YES completion:nil];
            }
        }
        else{
            
            [self presentViewController:[Utils showCustomAlert:@"Conection Error" andMessage:@"Please retry after some time"] animated:YES completion:nil];
        }
    }];
}

- (IBAction)latestPressed:(id)sender {
    
    [_latestBtn setTitleColor:[UIColor colorWithRed:0.816 green:(0.658) blue:(0.384) alpha:1.0] forState:UIControlStateNormal];
    [_favoriteBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_archiveBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    NSArray *viewsToRemove = [_upperScrollView subviews];
    for (UIView *v in viewsToRemove) {
        [v removeFromSuperview];
    }
    
    [self populateUpperScroll];
    
}

- (IBAction)favoritePressed:(id)sender {
    
    [_favoriteBtn setTitleColor:[UIColor colorWithRed:0.816 green:(0.658) blue:(0.384) alpha:1.0] forState:UIControlStateNormal];
    [_latestBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_archiveBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    
    NSArray *viewsToRemove = [_upperScrollView subviews];
    for (UIView *v in viewsToRemove) {
        [v removeFromSuperview];
    }
    
    favouriteArray = [[NSMutableArray alloc] init];
    
    for(int i=0; i<delegate.sharedUserModel.newsArray.count; i++) {
        News *nModel = [delegate.sharedUserModel.newsArray objectAtIndex:i];
        if([nModel.favourite boolValue]) {
            [favouriteArray addObject:nModel];
        }
    }
    
    [self populateUpperScrollForFavourite];
}

- (IBAction)eventDonePressed:(id)sender {
    _eventView.hidden = true;
}

- (IBAction)monthBtn:(id)sender {
    arr = [[NSArray alloc] init];
    arr = [NSArray arrayWithObjects:@"November 2016",@"December 2016",@"January 2017",@"Feburary 2017",@"March 2017",@"April 2017",@"May 2017",@"June 2017",@"July 2017",@"August 2017",@"September 2017",@"October 2017",@"November 2017",nil];
    NSArray * arrImage = nil;
    if(dropDown == nil) {
        CGFloat f = 180;
        dropDown = [[NIDropDown alloc]showDropDown:sender :&f :arr :arrImage :@"down"];
        dropDown.delegate = self;
    }
    else {
        [dropDown hideDropDown:sender];
        [self rel];
    }
}

- (IBAction)detailBackPressed:(id)sender {
    _detailView.hidden = true;
}

- (IBAction)readMorePressed:(id)sender {
    
    UIButton *btnSender = (UIButton*) sender;
    
    News *nModel = (News*)[delegate.sharedUserModel.newsArray objectAtIndex:btnSender.tag];
    
    _detailView.hidden = false;
    selectedNewsId = nModel.news_id;
    
    _detailImgView.image = [UIImage imageNamed:@"placeholder"];
    _detailImgView.imageURL = [NSURL URLWithString:nModel.picture];
    NSURL *url = [NSURL URLWithString:nModel.picture];
    [[AsyncImageLoader sharedLoader] loadImageWithURL:url];
    
    _detailTitleLbl.text = nModel.title;
    _detailTxt.text = nModel.detail;
    BOOL isFav = [nModel.favourite boolValue];
    
    if(isFav) {
        [_startBtn setImage:[UIImage imageNamed:@"star_filled"] forState:UIControlStateNormal];
    }
    else {
        [_startBtn setImage:[UIImage imageNamed:@"star_unfilled"] forState:UIControlStateNormal];
    }
}

- (IBAction)readMorePressedForFavourite:(id)sender {
    
    UIButton *btnSender = (UIButton*) sender;
    
    News *nModel = (News*)[favouriteArray objectAtIndex:btnSender.tag];
    
    _detailView.hidden = false;
    selectedNewsId = nModel.news_id;
    
    _detailImgView.image = [UIImage imageNamed:@"placeholder"];
    _detailImgView.imageURL = [NSURL URLWithString:nModel.picture];
    NSURL *url = [NSURL URLWithString:nModel.picture];
    [[AsyncImageLoader sharedLoader] loadImageWithURL:url];
    
    _detailTitleLbl.text = nModel.title;
    _detailTxt.text = nModel.detail;
    BOOL isFav = [nModel.favourite boolValue];
    
    if(isFav) {
        [_startBtn setImage:[UIImage imageNamed:@"star_filled"] forState:UIControlStateNormal];
    }
    else {
        [_startBtn setImage:[UIImage imageNamed:@"star_unfilled"] forState:UIControlStateNormal];
    }
}

- (IBAction)refreshPressed:(id)sender {
    [self sendRefreshCall];
}
- (void)sendRefreshCall {
    
    AppDelegate *del = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [CustomLoading showAlertMessage];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *date_time = [NSString stringWithFormat:@"%@", [dateFormatter stringFromDate: [NSDate date]]];
    
    NSString *userID = (NSString*)[[NSUserDefaults standardUserDefaults] objectForKey:@"userID"];
    NSString *secondPartOfUrl = [NSString stringWithFormat:@"%@%@",@"User/get_home_content?user_id=",userID];
    
    
    
    NSString *urlStr = [NSString stringWithFormat:@"%@%@",BASE_URL,secondPartOfUrl];
    
    urlStr = [NSString stringWithFormat:@"%@&date_time=%@",urlStr,date_time];
    if(del.deviceToken) {
        urlStr = [NSString stringWithFormat:@"%@&device_id=%@",urlStr,del.deviceToken];
        urlStr = [NSString stringWithFormat:@"%@&type=%@",urlStr,@"2"];
    }
    
    urlStr = [urlStr stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    NSURL *url = [NSURL URLWithString:urlStr];
    
    // Create the request.
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    // Create url connection and fire request
    NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    [CustomLoading showAlertMessage];
}
#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    // A response has been received, this is where we initialize the instance var you created
    // so that we can append data to it in the didReceiveData method
    // Furthermore, this method is called each time there is a redirect so reinitializing it
    // also serves to clear it
    _responseData = [[NSMutableData alloc] init];
    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    // Append the new data to the instance variable you declared
    [_responseData appendData:data];
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    // Return nil to indicate not necessary to store a cached response for this connection
    return nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    // The request is complete and data has been received
    // You can parse the stuff in your instance variable now
    
    [CustomLoading DismissAlertMessage];
    
    NSDictionary *result = [NSJSONSerialization JSONObjectWithData:_responseData options:0 error:nil];
    int flag = [[result objectForKey:@"status"] intValue];
    
    if(flag == 1) {
        
        User *user = [[User alloc] initWithDictionary:[result objectForKey:@"user_data"]];
        [user populateEvents:(NSArray*)[result objectForKey:@"events"]];
        [user populateBookedCallsTime:(NSArray*)[result objectForKey:@"user_specific_booked_calls"]];
        [user populateZodiacStarsArray:(NSArray*)[result objectForKey:@"hoscopesro"]];
        [user populateNewsArray:(NSArray*)[result objectForKey:@"news"]];
        [user populatePicturesArray:(NSArray*)[result objectForKey:@"pictures"]];
        [user populateWorkingHoursArray:(NSArray*)[result objectForKey:@"working_hours"]];
        [user populateVideoArray:(NSArray*)[result objectForKey:@"videos"]];
        [user populateTransactionArray:(NSArray*)[result objectForKey:@"transactions_history"]];
        CreditCard *ccObj = [[CreditCard alloc] initWithDictionary:[result objectForKey:@"credit_card_info"]];
        user.creditCardObj = ccObj;
        
        AppDelegate *appDel = (AppDelegate*)[UIApplication sharedApplication].delegate;
        appDel.sharedUserModel = user;
        
        [self populateUpperScroll];
        [self.eventTblView reloadData];
    }
    else {
        
        //[self presentViewController:[Utils showCustomAlert:@"حدث خطأ ما" andMessage:[result objectForKey:@"message"]] animated:YES completion:nil];
        
    }
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // The request has failed for some reason!
    // Check the error var
    [CustomLoading DismissAlertMessage];
    
    [self presentViewController:[Utils showCustomAlert:@"حدث خطأ ما" andMessage:@"There seems to be some problem with the connection"] animated:YES completion:nil];
}

- (IBAction)detailDonePressed:(id)sender {
    _detailView.hidden = true;
}

#pragma mark - Drop Down methods

- (void) niDropDownDelegateMethod: (NIDropDown *) sender {
    
    [self rel];
}

-(void)rel{
    //    [dropDown release];
    dropDown = nil;
}
@end

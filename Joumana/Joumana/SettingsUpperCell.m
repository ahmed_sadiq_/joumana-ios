//
//  SettingsUpperCell.m
//  Joumana
//
//  Created by Ahmed Sadiq on 22/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "SettingsUpperCell.h"

@implementation SettingsUpperCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

//
//  Horoscope.m
//  Joumana
//
//  Created by Ahmed Sadiq on 18/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "Horoscope.h"

@implementation Horoscope

- (id)initWithDictionary:(NSDictionary *) responseData
{
    
    self = [super init];
    
    if (self) {
        
        self.content = [self validStringForObject:responseData [@"content"]];
        self.date = [self validStringForObject:responseData [@"date"]];
        self.date_time = [self validStringForObject:responseData [@"date_time"]];
        self.horoscope_id = [self validStringForObject:responseData [@"id"]];
        self.star_id  = [self validStringForObject:responseData [@"star_id"]];
        self.val  = [self validStringForObject:responseData [@"val"]];
    }
    
    return self;
}


@end

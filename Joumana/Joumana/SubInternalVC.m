//
//  SubInternalVC.m
//  Joumana
//
//  Created by Ahmed Sadiq on 22/12/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "SubInternalVC.h"
#import "PictureModel.h"
#import "AsyncImageView.h"
#import "PictureModel.h"
#import "VideoModel.h"
#import "AudioModel.h"
@interface SubInternalVC ()<UIScrollViewDelegate>

@end

@implementation SubInternalVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.titleLbl.text = _pModel.title;
    self.descLbl.text = _pModel.desc;
    self.tfDesc.text = _pModel.desc;
    [self populateScrollView];
    [self.tfDesc setContentOffset:CGPointZero animated:NO];
    self.tfDesc.textContainerInset = UIEdgeInsetsZero;

    if(_option == 1) {
        _tLbl.text = @"القراءات اليومية";
    }
    else if(_option == 2) {
        _tLbl.text = @"القراءات الأسبوعية";
    }
    else if(_option == 3) {
        _tLbl.text = @"أرقام الحظ";
    }
    else if(_option == 10){
        _tLbl.text = @"الأبراج اليومية";
        
    }   else if(_option == 20){
        _tLbl.text = @"التوقعات السنوية";
        
    }
    else {
        _tLbl.text = @"نصيحة";
    }
}
-(void) viewDidLayoutSubviews{

}


- (void) populateScrollView {
    
    _players = [[NSMutableArray alloc] init];
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    
    int x=0;
    int y=0;
    
    for(int i=0; i<_pModel.pictures.count; i++) {
        AsyncImageView *asynImg = [[AsyncImageView alloc] initWithFrame:CGRectMake(x, 0, screenWidth, _mainScrollView.frame.size.height)];
        PictureModel *picModel = [_pModel.pictures objectAtIndex:i];
        
        asynImg.imageURL = [NSURL URLWithString:picModel.picture];
        NSURL *url = [NSURL URLWithString:picModel.picture];
        [[AsyncImageLoader sharedLoader] loadImageWithURL:url];
        [_mainScrollView addSubview:asynImg];
        x=x+screenWidth;
    }
    
    for(int i=0; i<_pModel.videos.count; i++) {
        
        VideoModel *vidModel = [_pModel.videos objectAtIndex:i];
        
        NSURL *url = [NSURL URLWithString:vidModel.video];
        
        GUIPlayerView *_playerView = [[GUIPlayerView alloc] initWithFrame:CGRectMake(x, 0, screenWidth, _mainScrollView.frame.size.height)];
        [_mainScrollView addSubview:_playerView];
        [_playerView setVideoURL:url];
        [_playerView prepareAndPlayAutomatically:YES];
        [_playerView setDelegate:self];
        [_playerView pause];
        [_players addObject:_playerView];
        x=x+screenWidth;
    }
    
    for(int i=0; i<_pModel.audio.count; i++) {
        
        AudioModel *vidModel = [_pModel.audio objectAtIndex:i];
        NSURL *url = [NSURL URLWithString:vidModel.audio];
        
        GUIPlayerView *_playerView = [[GUIPlayerView alloc] initWithFrame:CGRectMake(x, 0, screenWidth, _mainScrollView.frame.size.height)];
        [_mainScrollView addSubview:_playerView];
        [_playerView setVideoURL:url];
        [_playerView prepareAndPlayAutomatically:YES];
        [_playerView setDelegate:self];
        [_playerView pause];
        
        [_players addObject:_playerView];
        x=x+screenWidth;

    }
    _pageControl.numberOfPages = _pModel.audio.count + _pModel.videos.count + _pModel.pictures.count;
  
    _mainScrollView.contentSize = CGSizeMake(screenWidth*_pModel.pictures.count+screenWidth*_pModel.videos.count+screenWidth*_pModel.audio.count, _mainScrollView.frame.size.height);
    if (_pModel.audio.count==0 && _pModel.pictures.count == 0 && _pModel.videos.count ==0) {
        if ([_pModel.title isEqualToString:@""]) {
            _scrollViewHeightConstrain.constant = 0;
            _titLeTopConstrain.constant = 0;
            _titleHeightConstrain.constant = 0;
            _pageControl.hidden = YES;
        }
        else{
            _scrollViewTopConstrain.constant = 0;
            _scrollViewHeightConstrain.constant = 0;
            _titLeTopConstrain.constant = 30;

        }
    }
    if (_pageControl.numberOfPages == 1) {
        _pageControl.hidden = YES;
        
    }
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    int indexOfPage = _mainScrollView.contentOffset.x / _mainScrollView.frame.size.width;
    _pageControl.currentPage = indexOfPage;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backPressed:(id)sender {
    for(int i=0; i<_players.count; i++) {
        GUIPlayerView *pView = [_players objectAtIndex:i];
        [pView stop];
        [pView clean];

    }

    [self.navigationController popViewControllerAnimated:true];
}
@end

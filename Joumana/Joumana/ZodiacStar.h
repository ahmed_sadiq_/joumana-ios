//
//  ZodiacStar.h
//  Joumana
//
//  Created by Ahmed Sadiq on 18/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseEntity.h"
@interface ZodiacStar : BaseEntity

@property (nonatomic, strong) NSString * starName;
@property (nonatomic, strong) NSString * starId;
@property (nonatomic, strong) NSMutableArray * horoscopeArray;
@property (nonatomic, strong) NSString *isLikedBefore;
- (id)initWithDictionary:(NSDictionary *) responseData;

@end

//
//  TransactionHistoryCell.h
//  Joumana
//
//  Created by Ahmed Sadiq on 25/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TransactionHistoryCell : UITableViewCell


@property (strong, nonatomic) IBOutlet UILabel *dateLbl;
@property (strong, nonatomic) IBOutlet UILabel *timeLbl;
@property (strong, nonatomic) IBOutlet UILabel *amountLbl;

@end

//
//  GalleryVC.h
//  Joumana
//
//  Created by Ahmed Sadiq on 21/09/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "AsyncImageView.h"
#import <MediaPlayer/MediaPlayer.h>

@interface GalleryVC : UIViewController {
    AppDelegate *delegate;
    IBOutlet UILabel *imagesCount;
    IBOutlet UILabel *videoCount;
}
- (IBAction)videoBackPressed:(id)sender;


@property (strong, atomic) MPMoviePlayerController *moviePlayer;
@property (strong, nonatomic) IBOutlet UIScrollView *upperScroll;
@property (strong, nonatomic) IBOutlet UIScrollView *middleScroll;
@property (strong, nonatomic) IBOutlet UIScrollView *lowerScroll;
@property (strong, nonatomic) IBOutlet AsyncImageView *detailImg;

@property (strong, nonatomic) IBOutlet UIView *videoView;
@property (strong, nonatomic) IBOutlet UIView *videoPlayerView;


@property (strong, nonatomic) IBOutlet UIView *detailBg;
- (IBAction)detailDonePressed:(id)sender;


- (IBAction)openDetailPressed:(id)sender;

@end
